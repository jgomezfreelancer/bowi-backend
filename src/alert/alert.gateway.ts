import { Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import * as admin from 'firebase-admin';

import { User } from 'src/auth/user.entity';
import { UserRepository } from 'src/auth/user.repository';
import { Message } from 'src/message/message.entity';
import { MessageRepository } from 'src/message/message.repository';
import { ParticipantRepository } from 'src/participant/participant.repository';
import { Room } from 'src/room/room.entity';
import { RoomRepository } from 'src/room/room.repository';
import { NotificationRepository } from 'src/notification/notification.repository';
import { traslate } from 'src/helpers/traslate';

export interface CustomRoomFields extends Room {
  pendingViews?: number;
  lastMessage?: Message;
  user?: User;
  userPetition?: User;
  userImage?: string;
}

@WebSocketGateway({ namespace: '/chat-alert' })
export class AlertGateway implements OnGatewayInit {
  constructor(
    @InjectRepository(ParticipantRepository)
    private participantRepo: ParticipantRepository,
    @InjectRepository(RoomRepository)
    private roomRepo: RoomRepository,
    @InjectRepository(MessageRepository)
    private messageRepo: MessageRepository,
    @InjectRepository(UserRepository)
    private userRepo: UserRepository,
    @InjectRepository(NotificationRepository)
    private notificationRepository: NotificationRepository,
  ) {}

  private logger: Logger = new Logger('ChatAlertGateway');
  @WebSocketServer() wss: Server;

  afterInit(server: any) {
    this.logger.log('ChatAlertGateway Initialized!');
  }

  @SubscribeMessage('joinUserAlert')
  handleUserAlertJoin(client: Socket, payload: { userId: number }) {
    client.join(payload.userId);
    this.logger.log(`User ${payload.userId} has joined to Alet Gateway`);
    client.emit('joinedUserAlert', payload.userId);
    this.wss.emit('refreshLocations', { refresh: true });
  }

  @SubscribeMessage('leaveUserAlert')
  handleUserAlertLeave(client: Socket, payload: { userId: number }) {
    client.leave(payload.userId);
    this.logger.log(`User ${payload.userId} has left to Alet Gateway`);
    client.emit('leftRoom', payload.userId);
    this.wss.emit('refreshLocations', { refresh: true });
  }

  async incomingUserRequest(payload: { user: number; notification: object }) {
    this.wss.to(payload.user).emit('incomingUserRequest', payload.notification);
    this.logger.log(`incomingUserRequest to user ${payload.user}`);
  }

  async incomingMessage(payload: {
    room: number;
    userId: number;
    message: string;
    isMultimedia: boolean;
  }) {
    const userInverse = await this.checkInverseParticipant(
      payload.room,
      payload.userId,
    );
    console;
    const currentRoom: CustomRoomFields = await this.roomRepo.findOne({
      where: { id: payload.room },
      relations: ['petition', 'responsible', 'user_participant'],
    });

    if (currentRoom.petition) {
      const userPetition = await this.userRepo.findOne({
        id: currentRoom.petition.user_id,
      });
      currentRoom.userPetition = userPetition;
    }

    const currentUser = await this.participantRepo
      .createQueryBuilder('participant')
      .where('room_id = :room ', { room: payload.room })
      .where('user_id = :user', { user: payload.userId })
      .leftJoinAndSelect('participant.user', 'user')
      .leftJoinAndSelect('participant.room', 'room')
      .getOne();
    if (userInverse) {
      const notification = {
        isRequest: false,
        isAlert: true,
        room: currentRoom,
        id: userInverse.id,
        type: 'alert',
        title: `${currentUser?.user?.first_name} ${
          currentUser.user.last_name ? currentUser.user.last_name.charAt(0) : ''
        }`,
        text: payload.isMultimedia ? 'Photo' : payload.message,
        user: currentUser.user,
        smallIcon: 'drawable/icon',
        largeIcon: 'https://avatars2.githubusercontent.com/u/1174345?v=3&s=96',
        autoCancel: true,
        vibrate: [200, 300, 200, 300],
        color: '0000ff',
        headsUp: true,
        sound: true,
      };

      const fbPayload = {
        data: {
          notification: JSON.stringify(notification),
          priority: 'high',
        },
        notification: {
          title: `${currentUser?.user?.first_name} ${
            currentUser.user.last_name
              ? currentUser.user.last_name.charAt(0)
              : ''
          }`,
          body: payload.isMultimedia ? 'Photo' : payload.message,
        },
      };
      if (userInverse.fcm) {
        await admin
          .messaging()
          .sendToDevice(userInverse.fcm, fbPayload)
          .then(res => {
            if (res.failureCount === 0) {
              this.logger.debug(
                `Firebase Push Notification to user ${userInverse.username} type : New Message `,
              );
            } else {
              this.logger.debug(
                `Firebase Error to Sending Push Notification to user ${
                  userInverse.username
                },  error : ${JSON.stringify(res.results[0])}`,
              );
            }
          })
          .catch(err => err);
      } else {
        this.logger.debug(
          `Failed, fcm null to Send FirebasePush Notification to user ${userInverse.username} type : New Message  `,
        );
      }
    }
  }

  async incomingParticipantMessage(
    room: Room,
    responsible: User,
    participant: User,
    locale: string,
  ) {
    const language = traslate(participant.lang);
    if (participant) {
      const notification = {
        isRequest: false,
        isAlert: true,
        isPetition: false,
        room,
        id: participant.id,
        type: 'alert',
        title: `${language.chatCreadoCon} ${responsible.first_name} ${
          responsible.last_name ? responsible.last_name.charAt(0) : ''
        }`,
        text: '',
        user: responsible,
        smallIcon: 'drawable/icon',
        largeIcon: 'https://avatars2.githubusercontent.com/u/1174345?v=3&s=96',
        autoCancel: true,
        vibrate: [200, 300, 200, 300],
        color: '0000ff',
        headsUp: true,
        sound: true,
      };

      const payload = {
        data: {
          notification: JSON.stringify(notification),
          priority: 'high',
        },
        notification: {
          title: `${language.chatCreadoCon} ${responsible.first_name} ${
            responsible.last_name ? responsible.last_name.charAt(0) : ''
          }`,
          body: '',
        },
      };
      if (participant.fcm) {
        await admin
          .messaging()
          .sendToDevice(participant.fcm, payload)
          .then(res => {
            if (res.failureCount === 0) {
              this.logger.debug(
                `Firebase Send Push Notification to user ${participant.username} type : New Chat room Incomming Participant Message`,
              );
            } else {
              this.logger.debug(
                `Firebase Error to Sending Push Notification to user ${
                  participant.username
                },  error : ${JSON.stringify(res.results[0])}`,
              );
            }
          })
          .catch(err => err);
      } else {
        this.logger.debug(
          `Failed, fcm null to Send FirebasePush Notification to user ${participant.username} type : New Chat room Incomming Participant Message `,
        );
      }
    }
  }

  async getCountPendingViews(room: number, user: number): Promise<any> {
    const inverseUser = await this.checkInverseParticipant(room, user);
    if (inverseUser) {
      const countPendingViews = await this.messageRepo.find({
        room_id: room,
        user_id: inverseUser.id,
        status_view: 0,
      });
      return countPendingViews.length;
    }
    return 0;
  }

  async getRoomByUser(user: number): Promise<void[]> {
    const userRooms: CustomRoomFields[] = await this.roomRepo
      .createQueryBuilder('room')
      .leftJoinAndSelect('room.petition', 'petition')
      .getMany();
    const rooms = [];
    for (const [index, item] of userRooms.entries()) {
      const userId: number | null =
        item.responsible_id == user
          ? item.participant_id
          : item.participant_id == user
          ? item.responsible_id
          : null;
      if (userId) {
        const userRoom = await this.userRepo.findOne({
          where: { id: userId },
        });
        userRooms[index].user = userRoom;
        const message = await this.messageRepo.findOne({
          where: { room_id: item.id },
          order: { created: 'DESC' },
        });
        userRooms[index].lastMessage = message;
        userRooms[index].pendingViews = await this.getCountPendingViews(
          item.id,
          user,
        );
        if (item.petition) {
          const userPetition = await this.userRepo.findOne({
            where: { id: item.petition.user_id },
          });
          userRooms[index].userPetition = userPetition;
        }

        rooms.push(userRooms[index]);
      } else {
        delete userRooms[index];
      }
    }
    return rooms ? rooms : [];
  }

  async checkInverseParticipant(
    roomId: number,
    user: number,
  ): Promise<User | null> {
    const roomResponsible = await this.roomRepo.findOne({
      where: { id: roomId, participant_id: user },
      relations: ['responsible'],
    });
    if (roomResponsible) {
      return roomResponsible.responsible;
    }
    const roomParticipant = await this.roomRepo.findOne({
      where: { id: roomId, responsible_id: user },
      relations: ['user_participant'],
    });
    if (roomParticipant) {
      return roomParticipant.user_participant;
    }
    return null;
  }

  async incomingChatMessageRooms(payload: { room: number; userId: number }) {
    const inverse = await this.checkInverseParticipant(
      payload.room,
      payload.userId,
    );
    if (inverse) {
      const rooms = await this.getRoomByUser(inverse.id);
      this.wss.to(inverse.id).emit('clientRefreshRoomList', rooms);
      this.logger.log(`incomingChatMessageRooms to user ${inverse.username}`);
    }
  }

  async refreshRoomList(userId: number, username: string) {
    const rooms = await this.getRoomByUser(userId);
    this.wss.to(userId).emit('clientRefreshRoomList', rooms);
    this.logger.log(`Refresh rooms to User ${username}`);
  }

  updateLocations() {
    this.wss.emit('refreshLocations', { refresh: true });
  }
}
