import { Injectable } from '@nestjs/common';
import { AlertGateway } from './alert.gateway';

@Injectable()
export class AlertService {
    constructor(
        private alertGateway: AlertGateway
    ) {}

    async incomingUserRequest(payload) {
        this.alertGateway.incomingUserRequest(payload);
    }
}
