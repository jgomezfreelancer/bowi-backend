import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { AlertGateway } from './alert.gateway';

@Controller('alert')
export class AlertController {
  constructor(private alertGateway: AlertGateway) {}

  @Get('/manual-alert')
  manualAlert(@Query('user') userId: number, @Query('room') room: number) {
    this.alertGateway.incomingMessage({
      room,
      userId,
      message: 'Test',
      isMultimedia: false,
    });
  }
}
