import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParticipantRepository } from 'src/participant/participant.repository';
import { AlertGateway } from './alert.gateway';
import { AlertController } from './alert.controller';
import { AlertService } from './alert.service';
import { RoomRepository } from 'src/room/room.repository';
import { MessageRepository } from 'src/message/message.repository';
import { UserRepository } from 'src/auth/user.repository';
import { NotificationRepository } from 'src/notification/notification.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ParticipantRepository,
      RoomRepository,
      MessageRepository,
      UserRepository,
      NotificationRepository
    ]),
  ],
  providers: [AlertGateway, AlertService],
  controllers: [AlertController],
  exports: [AlertGateway],
})
export class AlertModule {}
