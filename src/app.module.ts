import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { AuthModule } from './auth/auth.module';
import { LanguageModule } from './language/language.module';
import { NationalityModule } from './nationality/nationality.module';
import { NotificationModule } from './notification/notification.module';
import { RoomModule } from './room/room.module';
import { MessageModule } from './message/message.module';
import { ParticipantModule } from './participant/participant.module';
import { ChatModule } from './chat/chat.module';
import { AlertModule } from './alert/alert.module';
import { CountryModule } from './country/country.module';
import { RankingModule } from './ranking/ranking.module';
import { MulterModule } from '@nestjs/platform-express';
import { UserLanguagesModule } from './user-languages/user-languages.module';
import { UserNationalitiesModule } from './user-nationalities/user-nationalities.module';
import { EmailModule } from './email/email.module';
import { PetitionModule } from './petition/petition.module';
import { ScheduleModule } from '@nestjs/schedule';
import { InterestModule } from './interest/interest.module';
import { UserInterestModule } from './user-interest/user-interest.module';
import { UserBlockModule } from './user-block/user-block.module';

@Module({
  imports: [
    MulterModule.register({
      dest: './src/images',
    }),
    TypeOrmModule.forRoot(typeOrmConfig),
    AuthModule,
    LanguageModule,
    NationalityModule,
    NotificationModule,
    RoomModule,
    MessageModule,
    ParticipantModule,
    ChatModule,
    AlertModule,
    CountryModule,
    RankingModule,
    UserLanguagesModule,
    UserNationalitiesModule,
    EmailModule,
    PetitionModule,
    ScheduleModule.forRoot(),
    InterestModule,
    UserInterestModule,
    UserBlockModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
