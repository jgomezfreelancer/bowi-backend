import en from 'src/utils/locales/en';
import es from 'src/utils/locales/es';

export const traslate = (locale: string | null) => {
  switch (locale) {
    case 'es':
      return es;
    case 'en':
      return en;
    default:
      return es;
  }
};
