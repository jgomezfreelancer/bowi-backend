import { Body, Controller, Get, Param, ParseIntPipe, Post } from '@nestjs/common';
import { CreateRankingDto } from './dto/create-ranking.dto';
import { Ranking } from './ranking.entity';
import { RankingService } from './ranking.service';

@Controller('ranking')
export class RankingController {
    constructor(
        private service: RankingService
    ){}

    @Get('/user/:id')
    async getRankingByUser(@Param('id', ParseIntPipe) id: number): Promise<any> {
        return this.service.getRankingByUser(id);
    }

    @Post('/')
    async store(@Body() body: CreateRankingDto): Promise<void> {
        return this.service.store(body);
    }
}
