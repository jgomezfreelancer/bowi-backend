import { EntityRepository, Repository } from 'typeorm';
import { CreateRankingDto } from './dto/create-ranking.dto';
import { Ranking } from './ranking.entity';

@EntityRepository(Ranking)
export class RankingRepository extends Repository<Ranking> {

  async store( body: CreateRankingDto): Promise<void> {
    const { user_id, type } = body;
    this.create({ user_id, type }).save();
  };

}
