import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateRankingDto } from './dto/create-ranking.dto';
import { Ranking } from './ranking.entity';
import { RankingRepository } from './ranking.repository';

@Injectable()
export class RankingService {
    constructor(
        @InjectRepository(RankingRepository)
        private repo : RankingRepository
    ){}

    async getResultByStart(user_id: number, type: number): Promise<{ count: number, result: number }> {
        const count = await this.repo.find({ where: { user_id, type } });
        const result =  count.length > 0 ? type * count.length : 0;
        return { count : count.length, result  }
    }

    async getRankingByUser(id: number): Promise<any> {

        const { count: start1, result: result1 } = await this.getResultByStart(id, 1);
        const { count: start2, result: result2 } = await this.getResultByStart(id, 2);
        const { count: start3, result: result3 } = await this.getResultByStart(id, 3);
        const { count: start4, result: result4 } = await this.getResultByStart(id, 4);
        const { count: start5, result: result5 } = await this.getResultByStart(id, 5);

        const countAllStars = start1 + start2 + start3 + start4 + start5;
        const countResultAllStars = result1 + result2 + result3 + result4 + result5;
        const ranking = countAllStars === 0 && countResultAllStars === 0 ? 0 : countResultAllStars / countAllStars;
        return ranking.toFixed(2);
    }

    async store(body: CreateRankingDto): Promise<void> {
        return this.repo.store(body);
    }
}
