import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, Unique } from 'typeorm';

@Entity()
export class Ranking extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
    
  @Column()
  user_id: number;

  @Column()
  type: number;

}
