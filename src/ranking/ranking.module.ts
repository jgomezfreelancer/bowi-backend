import { Module } from '@nestjs/common';
import { RankingService } from './ranking.service';
import { RankingController } from './ranking.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RankingRepository } from './ranking.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([RankingRepository])
  ],
  providers: [RankingService],
  controllers: [RankingController],
  exports: [RankingService]
})
export class RankingModule {}
