import { IsNotEmpty } from 'class-validator';

export class CreateRankingDto {
  @IsNotEmpty()
  user_id: number;
  
  @IsNotEmpty()
  type: number;
}
