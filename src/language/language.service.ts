import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLanguageDto } from './dto/create-language.dto';
import { Language } from './language.entity';
import * as fs from 'fs';

import { LanguageRepository } from './language.repository'

@Injectable()
export class LanguageService {
    constructor(
        @InjectRepository(LanguageRepository)
        private repo : LanguageRepository
    ){}

    async getLanguageImage(image: string): Promise<string | null> {
          try {
            const data: any = await fs.readFileSync(`./src/assets/flags/${image}`);
            const base64 = Buffer.from(data).toString('base64');
            const image64 = `data:image/png;base64,${base64}`;
            return image64;
          } catch (error) {
            return null;
          }
      }

    async index(): Promise<Language[]> {
        return this.repo.find({ order: { description: 'ASC' } });
    }

    async store(body: CreateLanguageDto): Promise<Language> {
        return this.repo.store(body);
    }
}
