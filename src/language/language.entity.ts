import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
} from 'typeorm';

@Entity()
@Unique(['description'])
export class Language extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: null, nullable: true })
  description: string;

  @Column({ default: null, nullable: true })
  image: string;

  @Column({ nullable: true, default: null })
  language: string;

  @Column({ default: null, nullable: true })
  slug: string;
}
