import { EntityRepository, Repository } from 'typeorm';
import { ConflictException, InternalServerErrorException } from '@nestjs/common';
import { Language } from './language.entity';
import { CreateLanguageDto } from './dto/create-language.dto';

@EntityRepository(Language)
export class LanguageRepository extends Repository<Language> {

  async store( body: CreateLanguageDto): Promise<Language> {
    const { description } = body;

    const language = new Language();
    language.description = description;
    try {
        return language.save();
      } catch (error) {
        if (error.code === '23505') { // duplicate
          throw new ConflictException('Language already exists');
        } else {
          throw new InternalServerErrorException();
        }
      }
  }

}
