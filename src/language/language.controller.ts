import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateLanguageDto } from './dto/create-language.dto';
import { Language } from './language.entity';
import { LanguageService } from './language.service';

@Controller('language')
export class LanguageController {
    constructor(
        private service: LanguageService
    ){}

    @Get('/')
    async index(): Promise<Language[]> {
        return this.service.index();
    }

    @Post('/')
    async store(@Body() body: CreateLanguageDto): Promise<Language> {
        return this.service.store(body);
    }
}
