import { Module } from '@nestjs/common';
import { LanguageService } from './language.service';
import { LanguageController } from './language.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LanguageRepository } from './language.repository';

@Module({
  imports:[
    TypeOrmModule.forFeature([LanguageRepository])
  ],
  providers: [LanguageService],
  controllers: [LanguageController]
})
export class LanguageModule {}
