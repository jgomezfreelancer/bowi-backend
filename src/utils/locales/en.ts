export default {
  chatCreadoCon: 'Chat created with',
  peticionActiva: 'Already has an active request',
  necesitaAyuda: 'needs help',
  quiereAyudarte: 'wants to help you',
  quiereHablarContigo: 'wants to talk to you',
};
