import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ENV } from '../env';

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: ENV.db.type,
  host: ENV.db.host,
  port: ENV.db.port,
  username: ENV.db.username,
  password: ENV.db.password,
  database: ENV.db.database,
  entities: [__dirname + '/../**/*.entity.{js,ts}'],
  synchronize: true,
};
