import { Body, Controller, Get, Post } from '@nestjs/common';
import { Country } from './country.entity';
import { CountryService } from './country.service';
import { CreateCountryDto } from './dto/create-country.dto';

@Controller('country')
export class CountryController {
    constructor(
        private service: CountryService
    ){}

    @Get('/')
    async index(): Promise<Country[]> {
        return this.service.index();
    }

    @Post('/')
    async store(@Body() body: CreateCountryDto): Promise<Country> {
        return this.service.store(body);
    }
}
