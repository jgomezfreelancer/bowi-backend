import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { Country } from './country.entity';
import { CreateCountryDto } from './dto/create-country.dto';

@EntityRepository(Country)
export class CountryRepository extends Repository<Country> {
  async store(body: CreateCountryDto): Promise<Country> {
    const { description } = body;

    const country = new Country();
    country.description = description;
    try {
      return country.save();
    } catch (error) {
      if (error.code === '23505') {
        // duplicate
        throw new ConflictException('Language already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }
}
