import { Module } from '@nestjs/common';
import { CountryService } from './country.service';
import { CountryController } from './country.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CountryRepository } from './country.repository';

@Module({
  imports: [TypeOrmModule.forFeature([CountryRepository])],
  providers: [CountryService],
  controllers: [CountryController],
})
export class CountryModule {}
