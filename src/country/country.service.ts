import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Country } from './country.entity';
import { CountryRepository } from './country.repository';
import { CreateCountryDto } from './dto/create-country.dto';

@Injectable()
export class CountryService {
  constructor(
    @InjectRepository(CountryRepository)
    private repo: CountryRepository,
  ) {}

  async index(): Promise<Country[]> {
    return this.repo.find();
  }

  async store(body: CreateCountryDto): Promise<Country> {
    return this.repo.store(body);
  }
}
