import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { CreateMessageDto } from './dto/crate-message.dto';
import { Message } from './message.entity';
import { MessageService } from './message.service';

@Controller('message')
export class MessageController {
  constructor(private service: MessageService) {}

  @Get('/')
  async index(): Promise<Message[]> {
    return this.service.index();
  }

  @Get('/room')
  async getRoomMessages(@Query('room') room: string, @Query('sender') sender: string ): Promise<Message[]> {
    return this.service.getRoomMessages(room, sender);
  }

  @Post('/')
  async store(@Body() body: CreateMessageDto): Promise<Message> {
    return this.service.store(body);
  }
}
