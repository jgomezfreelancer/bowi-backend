import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection } from 'typeorm';
import { CreateMessageDto } from './dto/crate-message.dto';
import { Message } from './message.entity';
import { MessageRepository } from './message.repository';

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(MessageRepository)
    private repo: MessageRepository,
  ) {}

  async index(): Promise<Message[]> {
    return this.repo.find();
  }

  async getRoomMessages(room_id: string, sender: string): Promise<Message[]> {
    await this.repo
      .createQueryBuilder('message')
      .update()
      .set({ status_view: 1 })
      .where('room_id = :room', { room: room_id })
      .where('user_id = :sender', { sender })
      .execute();

    return this.repo
      .createQueryBuilder('message')
      .where('room_id = :room', { room: room_id })
      .leftJoinAndSelect('message.user', 'user')
      .orderBy('created', 'ASC')
      .getMany();
  }

  async store(body: CreateMessageDto): Promise<Message> {
    return this.repo.store(body);
  }
}
