import { User } from 'src/auth/user.entity';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Message extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true, default: null })
  description: string;

  @Column({ nullable: true, default: null })
  room_id: number;

  @Column({ nullable: true, default: null })
  user_id: number;

  @Column({ nullable: true, default: null })
  status_view: number;

  @Column({ nullable: true, default: null })
  isMultimedia: boolean;

  @Column('timestamp with time zone')
  created: Date = new Date();

  @ManyToOne(
    type => User,
    user => user.participant,
    { cascade: true },
  )
  @JoinColumn({ name: 'user_id' })
  user: User;
}
