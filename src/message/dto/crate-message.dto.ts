import { IsNotEmpty } from 'class-validator';

export class CreateMessageDto {
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  room_id: number;

  @IsNotEmpty()
  user_id: number;
}
