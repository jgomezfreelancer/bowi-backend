import { EntityRepository, Repository } from 'typeorm';
import { InternalServerErrorException } from '@nestjs/common';
import { Message } from './message.entity';
import { CreateMessageDto } from './dto/crate-message.dto';

@EntityRepository(Message)
export class MessageRepository extends Repository<Message> {

  async store( body: CreateMessageDto): Promise<Message> {
    const { description, room_id, user_id } = body;

    const message = new Message();
    message.description = description;
    message.room_id = room_id;
    message.user_id = user_id;
    try {
        return message.save();
      } catch (error) {        
          throw new InternalServerErrorException();
      }
  }

}
