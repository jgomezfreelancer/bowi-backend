import { Module } from '@nestjs/common';
import { NationalityService } from './nationality.service';
import { NationalityController } from './nationality.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NationalityRepository } from './nationality.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([NationalityRepository])
  ],
  providers: [NationalityService],
  controllers: [NationalityController]
})
export class NationalityModule {}
