import { EntityRepository, Repository } from 'typeorm';
import { ConflictException, InternalServerErrorException } from '@nestjs/common';
import { Nationality } from './nationality.entity';
import { CreateNationalityDto } from './dto/create-nationality.dto';

@EntityRepository(Nationality)
export class NationalityRepository extends Repository<Nationality> {

  async store( body: CreateNationalityDto): Promise<Nationality> {
    const { description } = body;

    const nationality = new Nationality();
    nationality.description = description;
    try {
        return nationality.save();
      } catch (error) {
        if (error.code === '23505') { // duplicate
          throw new ConflictException('Nationality already exists');
        } else {
          throw new InternalServerErrorException();
        }
      }
  }

}
