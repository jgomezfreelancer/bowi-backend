import { IsNotEmpty } from 'class-validator';

export class CreateNationalityDto {
  @IsNotEmpty()
  description: string;
}
