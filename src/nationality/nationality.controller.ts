import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateNationalityDto } from './dto/create-nationality.dto';
import { Nationality } from './nationality.entity';
import { NationalityService } from './nationality.service';

@Controller('nationality')
export class NationalityController {
    constructor(
        private service: NationalityService
    ){}

    @Get('/')
    async index(): Promise<Nationality[]> {
        return this.service.index();
    }

    @Post('/')
    async store(@Body() body: CreateNationalityDto): Promise<Nationality> {
        return this.service.store(body);
    }
}
