import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateNationalityDto } from './dto/create-nationality.dto';
import { Nationality } from './nationality.entity';
import { NationalityRepository } from './nationality.repository';
import * as fs from 'fs';

@Injectable()
export class NationalityService {
    constructor(
        @InjectRepository(NationalityRepository)
        private repo : NationalityRepository
    ){}


    async getImage(image: string): Promise<string | null> {
        try {
          const data: any = await fs.readFileSync(`./src/assets/flags/${image}`);
          const base64 = Buffer.from(data).toString('base64');
          const image64 = `data:image/png;base64,${base64}`;
          return image64;
        } catch (error) {
          return null;
        }
    }


    async index(): Promise<Nationality[]> {
        return this.repo.find({ order: { description: 'ASC' } });
    }

    async store(body: CreateNationalityDto): Promise<Nationality> {
        return this.repo.store(body);
    }
}
