import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  Get,
  UseGuards,
  Query,
  Req,
  UnauthorizedException,
  Param,
  Put,
  Logger,
  Res,
} from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthService, CustomUserFields } from './auth.service';
import { AuthLoginCredentialsDto } from './dto/login-credentials.dto';
import { SocialNetworkCredentialsDto } from './dto/social-network-singup-dto';
import { JwtAuthGuard } from './Guards/auth-jwt.guard';
import { JwtService } from '@nestjs/jwt';
import { UpdateSettingsUserDto } from './dto/update-settings-user.dto';
import { UpdateCoordUserDto } from './dto/update-coord.dto';
import { UpdateFcmUserDto } from './dto/update-fcm.dto';
import { SendHelpUserDto } from './dto/send-help.dto';
import { GetUsersByLocationDto } from './dto/users-location.dto';
import { AcceptHelpUserDto } from './dto/accept-help.dto';
import { UpdateProfileUserDto } from './dto/update-profile.dto';
import { ICheckLogin } from './interfaces/check-login.interface';
import * as fs from 'fs';
import { Buffer } from 'buffer';
import { User } from './user.entity';
import { UpdateStarterSettingsDto } from './dto/update-starter-settings.dto';
import { UpdateLanguagesNationalitiesDto } from './dto/update-nationalities-languages.dto';
import { Petition } from 'src/petition/petition.entity';
import { Request } from 'express';
import { UserInterest } from 'src/user-interest/user-interest.entity';
import { CreateUserInterestDto } from 'src/user-interest/dto/create-user-interest.dto';
import { ENV } from 'src/env';

@Controller('auth')
export class AuthController {
  private logger = new Logger('AuthController');

  constructor(
    private authService: AuthService,
    private jwtService: JwtService,
  ) {}

  @Post('/signup')
  signUp(
    @Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto,
    @Req() req: Request,
  ): Promise<{ accessToken: string; user: User }> {
    const locale: any = req.headers.locale ? req.headers.locale : null;
    return this.authService.signUp(authCredentialsDto, locale);
  }

  @Post('/social-signup')
  socialNetWorkSignUp(
    @Body(ValidationPipe) authCredentialsDto: SocialNetworkCredentialsDto,
    @Req() req: Request,
  ): Promise<{ accessToken: string; user: CustomUserFields }> {
    const locale: any = req.headers.locale ? req.headers.locale : null;
    return this.authService.socialNetworkSignUp(authCredentialsDto, locale);
  }

  @Post('/signin')
  signIn(
    @Body(ValidationPipe) authCredentialsDto: AuthLoginCredentialsDto,
    @Req() req: Request,
  ): Promise<{ accessToken: string; user: CustomUserFields }> {
    const locale: any = req.headers.locale ? req.headers.locale : null;
    return this.authService.signIn(authCredentialsDto, locale);
  }

  @Get('/check')
  @UseGuards(JwtAuthGuard)
  check(@Req() req: Request): Promise<ICheckLogin> {
    const { authorization: accessToken } = req.headers;
    try {
      const decoded = this.jwtService.verify(
        accessToken.replace('Bearer ', ''),
      );
      const user = this.authService.getUsertoSingin(decoded.username);
      return user;
    } catch (ex) {
      throw new UnauthorizedException();
    }
  }

  @Put('/update-lang-nationality/:id')
  @UseGuards(JwtAuthGuard)
  updateLanguagesNationalitys(
    @Param('id') id: number,
    @Body() body: UpdateLanguagesNationalitiesDto,
  ) {
    return this.authService.updateLanguagesNationalitys(id, body);
  }

  @Put('/update-user/:id')
  @UseGuards(JwtAuthGuard)
  updateUser(@Param('id') id: number, @Body() body: UpdateSettingsUserDto) {
    return this.authService.updateUser(id, body);
  }

  @Put('/update-starter/:id')
  @UseGuards(JwtAuthGuard)
  updateStarterSettings(
    @Param('id') id: number,
    @Body() body: UpdateStarterSettingsDto,
  ) {
    return this.authService.updateStarterSettings(id, body);
  }

  @Put('/update-coord/:id')
  @UseGuards(JwtAuthGuard)
  updateCoord(@Param('id') id: number, @Body() body: UpdateCoordUserDto) {
    return this.authService.updateCoord(id, body);
  }

  @Put('/update-fcm/:id')
  updateFcm(@Param('id') id: number, @Body() body: UpdateFcmUserDto) {
    return this.authService.updateFcm(id, body);
  }

  @Post('/send-help')
  @UseGuards(JwtAuthGuard)
  async sendHelp(
    @Body() body: SendHelpUserDto,
    @Req() req: Request,
  ): Promise<any> {
    const locale: any = req.headers.locale ? req.headers.locale : null;
    return this.authService.sendHelp(body, locale);
  }

  @Post('/accept-help')
  @UseGuards(JwtAuthGuard)
  async acceptHelp(
    @Body() body: AcceptHelpUserDto,
    @Req() req: Request,
  ): Promise<any> {
    const locale: any = req.headers.locale ? req.headers.locale : null;
    return this.authService.acceptHelp(body, locale);
  }

  @Get('/users-by-location')
  async getUsersByLocation(
    @Query(ValidationPipe) body: GetUsersByLocationDto,
  ): Promise<any> {
    return this.authService.getUsersByLocation(body);
  }

  @Put('/update-profile/:id')
  @UseGuards(JwtAuthGuard)
  updateProfileUser(
    @Param('id') id: number,
    @Body() body: UpdateProfileUserDto,
  ) {
    return this.authService.updateProfileUser(id, body);
  }

  @Get('/user-image/:id')
  async getUserImage(
    @Param('id') id: number,
    @Res() res,
  ): Promise<string | null> {
    const image = await this.authService.getUserImage(id);
    if (image) {
      fs.readFile(`./src/images/${image}`, (err, data) => {
        const base64 = Buffer.from(data).toString('base64');
        const image = `data:image/png;base64,${base64}`;
        return res.send(image);
      });
    }
    return null;
  }

  @Get('/server-date')
  checkServerDate(): Date {
    return new Date();
  }

  @Get('/user-petition/:id')
  async getUserPetition(@Param('id') id: number): Promise<Petition> {
    return this.authService.getUserPetition(id);
  }

  @Get('/profile-image/:id')
  async getUserProfileImage(
    @Param('id') id: number,
    @Res() res,
  ): Promise<string | null> {
    const image = await this.authService.getUserImage(id);
    if (image) {
      fs.readFile(`./src/images/${image}`, (err, data) => {
        const base64 = Buffer.from(data).toString('base64');
        const image = `data:image/png;base64,${base64}`;
        return res.send(image);
      });
    }
    return null;
  }

  @Get('/user-disable-petition/:id')
  async disablePetition(@Param('id') id: number) {
    return this.authService.disablePetition(id);
  }

  @Get('/logout/:token')
  async logout(@Param('token') token: string) {
    if (token) {
      const decoded = this.jwtService.decode(token.replace('Bearer ', ''));
      this.authService.logout(decoded['username']);
    }
    return true;
  }

  @Post('/update-interest')
  @UseGuards(JwtAuthGuard)
  async updateUserInterest(@Body() body: CreateUserInterestDto): Promise<any> {
    return this.authService.updateUserInterest(body);
  }
}
