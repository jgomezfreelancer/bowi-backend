import { IsString, IsNotEmpty } from 'class-validator';

export class SocialNetworkCredentialsDto {
  @IsString()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  first_name?: string;

  @IsString()
  @IsNotEmpty()
  last_name?: string;
}
