import { IsNumber, IsString } from 'class-validator';

export class UpdateSettingsUserDto {
  @IsNumber()
  language_id: number;
  
  @IsNumber()
  nationality_id: number;

  @IsNumber()
  location: number;

  @IsNumber()
  profile: number;

  @IsNumber()
  limit_distance: number;
}
