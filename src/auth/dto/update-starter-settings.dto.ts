import { IsArray, IsNumber, IsString } from 'class-validator';

export class UpdateStarterSettingsDto {
  @IsArray()
  nationalities: number[];

  @IsArray()
  languages: number[];

  @IsNumber()
  location: number;

  @IsNumber()
  profile: number;

  @IsString()
  limit_distance: number;

  @IsString()
  nationality_id: number;

  @IsArray()
  interests: number[];
}
