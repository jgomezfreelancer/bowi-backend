import { IsNotEmpty } from 'class-validator';

export class GetUsersByLocationDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  km: number;
}
