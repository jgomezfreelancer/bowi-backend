import { IsNumber, IsString } from 'class-validator';

export class UpdateProfileUserDto {
  @IsString()
  first_name: string;

  @IsString()
  last_name: string;

  @IsNumber()
  age: number;

  @IsString()
  address: string;

  @IsString()
  cp: string;

  @IsNumber()
  country_id: number;

  @IsString()
  country_location: string;

  @IsNumber()
  language_id: number;

  @IsString()
  username: string;

  @IsString()
  password: string;

  @IsString()
  file: any;
}
