import { IsNumber, IsString } from 'class-validator';

export class AcceptHelpUserDto {
  @IsNumber()
  requestId: number;

  @IsNumber()
  acceptId: number;

  @IsString()
  message: string;

  @IsNumber()
  isPetition: number;

}
