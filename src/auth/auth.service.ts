import {
  Injectable,
  UnauthorizedException,
  Logger,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as admin from 'firebase-admin';
import { getDistance } from 'geolib';
import * as bcrypt from 'bcryptjs';
import * as moment from 'moment';

import { UserRepository } from './user.repository';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { JwtPayload } from './jwt-payload.interface';
import { User } from './user.entity';
import { AuthLoginCredentialsDto } from './dto/login-credentials.dto';
import { SocialNetworkCredentialsDto } from './dto/social-network-singup-dto';
import { UpdateSettingsUserDto } from './dto/update-settings-user.dto';
import { UpdateCoordUserDto } from './dto/update-coord.dto';
import { UpdateFcmUserDto } from './dto/update-fcm.dto';
import { GetUsersByLocationDto } from './dto/users-location.dto';
import { SendHelpUserDto } from './dto/send-help.dto';
import { AcceptHelpUserDto } from './dto/accept-help.dto';
import { NotificationRepository } from 'src/notification/notification.repository';
import { CreateNotificationDto } from 'src/notification/dto/create.dto';
import { AlertGateway } from 'src/alert/alert.gateway';
import { UpdateProfileUserDto } from './dto/update-profile.dto';
import { RankingService } from 'src/ranking/ranking.service';
import { ICheckLogin } from './interfaces/check-login.interface';
import { getImageName } from './dto/image-utils';
import * as fs from 'fs';
import { UpdateStarterSettingsDto } from './dto/update-starter-settings.dto';
import { UserLanguagesRepository } from 'src/user-languages/user-languages.repository';
import { UserNationalitiesRepository } from 'src/user-nationalities/user-nationalities.respository';
import { UpdateLanguagesNationalitiesDto } from './dto/update-nationalities-languages.dto';
import { PetitionRepository } from 'src/petition/petition.repository';
import { RoomRepository } from 'src/room/room.repository';
import { In } from 'typeorm';
import { Petition } from 'src/petition/petition.entity';
import { Cron } from '@nestjs/schedule';
import { UserInterestService } from 'src/user-interest/user-interest.service';
import { CreateUserInterestDto } from 'src/user-interest/dto/create-user-interest.dto';
import { UserBlockRepository } from 'src/user-block/user-block.repository';
import { traslate } from 'src/helpers/traslate';

export interface CustomUserFields extends User {
  petition?: Petition;
  languages?: number[];
  nationalities?: number[];
  profileImage?: string | null;
  ranking?: string;
  userInterestKeys?: number[];
}
@Injectable()
export class AuthService {
  private logger = new Logger('AuthService');

  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    @InjectRepository(NotificationRepository)
    private notificationRepository: NotificationRepository,
    private jwtService: JwtService,
    private alertGateway: AlertGateway,
    private rankingService: RankingService,
    @InjectRepository(UserLanguagesRepository)
    private userLanguagesRepository: UserLanguagesRepository,
    @InjectRepository(UserNationalitiesRepository)
    private userNationalitiesRepository: UserNationalitiesRepository,
    @InjectRepository(PetitionRepository)
    private petitionRepo: PetitionRepository,
    @InjectRepository(RoomRepository)
    private roomnRepo: RoomRepository,
    private userInterestService: UserInterestService,
    @InjectRepository(UserBlockRepository)
    private userBlockRepo: UserBlockRepository,
  ) {}

  async signUp(
    authCredentialsDto: AuthCredentialsDto,
    locale: string,
  ): Promise<{ accessToken: string; user: User }> {
    const user = await this.userRepository.signUp(authCredentialsDto, locale);
    const payload: JwtPayload = { username: user.username };
    const accessToken = await this.jwtService.sign(payload);

    delete user.password;
    delete user.salt;

    this.logger.debug(
      `Generated JWT Token with payload ${JSON.stringify(payload)}`,
    );
    this.alertGateway.updateLocations();
    return { accessToken, user };
  }

  async socialNetworkSignUp(
    authCredentialsDto: SocialNetworkCredentialsDto,
    locale: string,
  ): Promise<{ accessToken: string; user: CustomUserFields }> {
    const { email } = authCredentialsDto;
    const existingUser: User = await this.userRepository.checkSocialNetworkUser(
      email,
    );

    const user: CustomUserFields = existingUser
      ? existingUser
      : await this.userRepository.socialNetworkSignUp(
          authCredentialsDto,
          locale,
        );
    await this.userRepository.update(user.id, {
      connected: true,
      lang: locale,
    });
    const payload: JwtPayload = { username: user.username };
    const accessToken = await this.jwtService.sign(payload);

    delete user.password;
    delete user.salt;
    const ranking = await this.rankingService.getRankingByUser(user.id);
    user.ranking = ranking;

    const userInterestKeys = user.userToInterest
      ? user.userToInterest.map(element => element.interestId)
      : [];
    user.userInterestKeys = userInterestKeys;

    const languages = await this.userLanguagesRepository.find({
      where: { user_id: user.id },
    });
    if (languages.length > 0) {
      const languagesArrayKeys = languages.map(element => element.language_id);
      user.languages = languagesArrayKeys;
    }
    this.logger.debug(
      `Generated JWT Token with payload ${JSON.stringify(payload)}`,
    );

    return { accessToken, user };
  }

  async signIn(
    authCredentialsDto: AuthLoginCredentialsDto,
    locale: string,
  ): Promise<{ accessToken: string; user: CustomUserFields }> {
    const user: CustomUserFields = await this.userRepository.validateUserPassword(
      authCredentialsDto,
    );

    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    }

    const payload: JwtPayload = { username: user.username };
    const accessToken = await this.jwtService.sign(payload);
    delete user.password;
    delete user.salt;

    await this.userRepository.update(user.id, { lang: locale });

    const userInterestKeys = user.userToInterest
      ? user.userToInterest.map(element => element.interestId)
      : [];
    user.userInterestKeys = userInterestKeys;

    const languages = await this.userLanguagesRepository.find({
      where: { user_id: user.id },
    });
    if (languages.length > 0) {
      const languagesArrayKeys = languages.map(element => element.language_id);
      user.languages = languagesArrayKeys;
    }

    const nationalities = await this.userNationalitiesRepository.find({
      where: { user_id: user.id },
    });
    if (nationalities.length > 0) {
      const nationalitiesArrayKeys = nationalities.map(
        element => element.nationality_id,
      );
      user.nationalities = nationalitiesArrayKeys;
    }
    const ranking = await this.rankingService.getRankingByUser(user.id);
    user.ranking = ranking;
    await this.userRepository.update(user.id, { connected: true });
    this.logger.debug(
      `Generated JWT Token with payload ${JSON.stringify(payload)}`,
    );

    return { accessToken, user };
  }

  async getUsertoSingin(username: string): Promise<ICheckLogin> {
    const current: User = await this.userRepository.findOne({
      where: { username },
    });
    const user: CustomUserFields = await this.getUserById(current.id);
    const ranking = await this.rankingService.getRankingByUser(user.id);

    const userInterestKeys = user.userToInterest
      ? user.userToInterest.map(element => element.interestId)
      : [];
    user.userInterestKeys = userInterestKeys;

    const languages = await this.userLanguagesRepository.find({
      where: { user_id: current.id },
    });
    if (languages.length > 0) {
      const languagesArrayKeys = languages.map(element => element.language_id);
      user.languages = languagesArrayKeys;
    }

    const nationalities = await this.userNationalitiesRepository.find({
      where: { user_id: current.id },
    });
    if (nationalities.length > 0) {
      const nationalitiesArrayKeys = nationalities.map(
        element => element.nationality_id,
      );
      user.nationalities = nationalitiesArrayKeys;
    }
    await this.userRepository.update(user.id, { connected: true });
    return { ...user, ranking };
  }

  async getUserById(id: number): Promise<User> {
    const found: CustomUserFields = await this.userRepository.findOne({
      where: { id },
      relations: ['userToInterest'],
    });
    if (!found) {
      throw new NotFoundException(`User with ID "${id}" not found`);
    }
    delete found.password;
    delete found.salt;
    const userInterestKeys = found.userToInterest
      ? found.userToInterest.map(element => element.interestId)
      : [];
    found.userInterestKeys = userInterestKeys;
    const languages = await this.userLanguagesRepository.find({
      where: { user_id: id },
    });
    if (languages.length > 0) {
      const languagesArrayKeys = languages.map(element => element.language_id);
      found.languages = languagesArrayKeys;
    }

    const nationalities = await this.userNationalitiesRepository.find({
      where: { user_id: id },
    });
    if (nationalities.length > 0) {
      const nationalitiesArrayKeys = nationalities.map(
        element => element.nationality_id,
      );
      found.nationalities = nationalitiesArrayKeys;
    }

    return found;
  }

  async updateUser(id: number, body: UpdateSettingsUserDto) {
    this.userRepository.update(id, body);
    const ranking = await this.rankingService.getRankingByUser(id);
    const user = await this.getUserById(id);
    this.alertGateway.updateLocations();
    return { ...user, ranking };
  }

  async updateLanguagesNationalitys(
    id: number,
    body: UpdateLanguagesNationalitiesDto,
  ) {
    const { languages, nationality_id } = body;
    if (languages.length > 0) {
      this.userLanguagesRepository
        .createQueryBuilder()
        .delete()
        .where('user_id = :id', { id })
        .execute();
      for (const [index, item] of languages.entries()) {
        await this.userLanguagesRepository
          .create({
            user_id: id,
            language_id: item,
          })
          .save();
      }
    }

    this.userRepository.update(id, { nationality_id });

    const ranking = await this.rankingService.getRankingByUser(id);
    const user = await this.getUserById(id);
    this.alertGateway.updateLocations();
    return { ...user, ranking };
  }

  async updateStarterSettings(user_id: number, body: UpdateStarterSettingsDto) {
    const {
      location,
      profile,
      nationalities,
      languages,
      limit_distance,
      nationality_id,
      interests,
    } = body;
    this.userRepository.update(user_id, {
      location,
      profile,
      limit_distance,
      first_time: false,
    });
    if (interests.length > 0) {
      await this.userInterestService.store({
        userId: user_id,
        interests,
      });
    }
    if (languages.length > 0) {
      await this.userLanguagesRepository
        .createQueryBuilder()
        .delete()
        .where('user_id = :user_id', { user_id })
        .execute();
      for (const [index, item] of languages.entries()) {
        await this.userLanguagesRepository
          .create({
            user_id,
            language_id: item,
          })
          .save();
      }
    }

    this.userRepository.update(user_id, { nationality_id });

    const ranking = await this.rankingService.getRankingByUser(user_id);
    const user = await this.getUserById(user_id);
    this.alertGateway.updateLocations();
    return { ...user, ranking };
  }

  async updateCoord(id: number, body: UpdateCoordUserDto): Promise<void> {
    const { latitude, longitude } = body;
    this.userRepository.update(id, { latitude, longitude });
    const user = await this.getUserById(id);
    this.alertGateway.updateLocations();
    this.logger.debug(`Coord updated to user ${user.username}`);
  }

  async updateFcm(id: number, body: UpdateFcmUserDto): Promise<User> {
    const { fcm } = body;
    this.userRepository.update(id, { fcm });
    const user = await this.getUserById(id);
    this.logger.debug(`FCM updated to user ${user.username}`);
    return user;
  }

  async checkCurrentRoom(
    responsible: number,
    participant: number,
  ): Promise<boolean> {
    const attemp1 = await this.roomnRepo.findOne({
      responsible_id: responsible,
      participant_id: participant,
      disable: false,
    });
    if (attemp1) {
      return true;
    }
    const attemp2 = await this.roomnRepo.findOne({
      responsible_id: participant,
      participant_id: responsible,
      disable: false,
    });
    if (attemp2) {
      return true;
    }
    return false;
  }

  async checkBlockUser(authUser: number, blockUser: number): Promise<boolean> {
    const user = await this.userBlockRepo.findOne({
      authUser,
      blockUser,
    });
    if (user) {
      return true;
    }
    const userInverse = await this.userBlockRepo.findOne({
      authUser: blockUser,
      blockUser: authUser,
    });
    if (userInverse) {
      return true;
    }
    return false;
  }

  async getUsersByLocation(body: GetUsersByLocationDto): Promise<any> {
    const { id, km } = body;
    const meters = km * 1000;
    const user: CustomUserFields = await this.getUserById(id);
    const location = { latitude: user.latitude, longitude: user.longitude };
    const coords = [];

    const usersEntity = this.userRepository.createQueryBuilder('user');
    usersEntity.where('profile = :profile', { profile: 1 });
    usersEntity.where('connected = :connected', { connected: true });

    if (user.nationality_id) {
      usersEntity.where('nationality_id = :nationality_id', {
        nationality_id: user.nationality_id,
      });
    }

    if (user.userToInterest.length) {
      usersEntity
        .leftJoinAndSelect('user.userToInterest', 'user_interest', '')
        .where('user_interest.interestId IN (:...interestId)', {
          interestId: user.userInterestKeys,
        });
    }

    const users: CustomUserFields[] = await usersEntity.getMany();
    for (const [index, element] of users.entries()) {
      const blockUser = await this.checkBlockUser(id, element.id);
      if (
        id != element.id &&
        element.latitude &&
        element.longitude &&
        !blockUser
      ) {
        const userCoord = {
          latitude: parseFloat(element.latitude),
          longitude: parseFloat(element.longitude),
        };
        const userMeters = getDistance(location, userCoord);
        if (userMeters <= meters) {
          const petition = await this.petitionRepo.findOne({
            user_id: element.id,
            status: 1,
          });
          const languagesAndNationalities = [];
          users[index].petition = petition ? petition : null;
          const profileImage = await this.getProfileImage(element.id);
          users[index].profileImage = profileImage ? profileImage : null;
          if (user.languages) {
            const matchLanguages = await this.userLanguagesRepository.find({
              where: { user_id: element.id, language_id: In(user.languages) },
            });
            if (matchLanguages) {
              languagesAndNationalities.push(1);
            }
            if (languagesAndNationalities.length > 0) {
              coords.push({ ...element, meters: userMeters });
            }
          } else {
            coords.push({ ...element, meters: userMeters });
          }
        }
      }
    }
    return coords;
  }

  async getImage(id: number): Promise<string | null> {
    const user = await this.userRepository.findOne({ where: { id } });
    return user.picture ? user.picture : null;
  }

  async getProfileImage(id: number): Promise<string | null> {
    const image = await this.getImage(id);
    if (image !== null) {
      try {
        const data: any = await fs.readFileSync(`./src/images/${image}`);
        const base64 = Buffer.from(data).toString('base64');
        const image64 = `data:image/png;base64,${base64}`;
        return image64;
      } catch (error) {
        return null;
      }
    }
    return null;
  }

  async sendHelp(
    body: SendHelpUserDto,
    locale: string | null,
  ): Promise<boolean> {
    const { id, message, km, interests } = body;
    const language = traslate(locale);
    const meters = km * 1000;
    const user: CustomUserFields = await this.getUserById(id);
    const checkPetition = await this.petitionRepo.findOne({
      where: { user_id: id, status: 1 },
    });

    if (checkPetition) {
      throw new BadRequestException('Ya posee una peticion activa');
    }
    const userConditions: { nationality_id?: number } = {};
    if (user.nationality_id) {
      userConditions.nationality_id = user.nationality_id;
    }

    const usersEntity = this.userRepository.createQueryBuilder('user');
    usersEntity.where('profile = :profile', { profile: 1 });
    usersEntity.where('connected = :connected', { connected: true });

    if (user.nationality_id) {
      usersEntity.where('nationality_id = :nationality_id', {
        nationality_id: user.nationality_id,
      });
    }
    if (interests.length > 0) {
      usersEntity
        .leftJoinAndSelect('user.userToInterest', 'user_interest', '')
        .where('user_interest.interestId IN (:...interestId)', {
          interestId: interests,
        });
    }

    const users = await usersEntity.getMany();

    const location = {
      latitude: parseFloat(user.latitude),
      longitude: parseFloat(user.longitude),
    };
    const petition = await this.petitionRepo
      .create({
        description: message,
        user_id: id,
        status: 1,
      })
      .save();
    user.petition = petition ? petition : null;
    const registrationTokens = [];
    for (const item of users) {
      const blockUser = await this.checkBlockUser(id, item.id);
      if (user.id !== item.id && !blockUser) {
        const userCoord = {
          latitude: parseFloat(item.latitude),
          longitude: parseFloat(item.longitude),
        };
        const userMeters = getDistance(location, userCoord);

        if (item.fcm) {
          if (userMeters <= meters) {
            if (user.languages) {
              const matchLanguages = await this.userLanguagesRepository.find({
                where: { user_id: item.id, language_id: In(user.languages) },
              });
              if (matchLanguages) {
                registrationTokens.push(item.fcm);
                this.logger.debug(
                  `Send Push Notification to user ${item.username} type request with distance ${userMeters}`,
                );
              }
            } else {
              registrationTokens.push(item.fcm);
              this.logger.debug(
                `Send Push Notification to user ${item.username} type request with distance ${userMeters}`,
              );
            }
          }
        }
      }
    }
    if (registrationTokens.length > 0) {
      const ranking = await this.rankingService.getRankingByUser(user.id);
      const dataNotification = {
        isRequest: true,
        isAlert: false,
        isPetition: true,
        id,
        type: 'request',
        user: { ...user, ranking },
        smallIcon: 'drawable/icon',
        largeIcon: 'https://avatars2.githubusercontent.com/u/1174345?v=3&s=96',
        autoCancel: true,
        vibrate: [200, 300, 200, 300],
        color: '0000ff',
        headsUp: true,
        sound: true,
      };
      const payload = {
        data: {
          notification: JSON.stringify(dataNotification),
          priority: 'high',
        },
        notification: {
          title: `${user.first_name} ${
            user.last_name ? user.last_name.charAt(0) : ''
          } necesita ayuda`,
          body: message,
          content: JSON.stringify(dataNotification),
        },
      };
      await admin
        .messaging()
        .sendToDevice(registrationTokens, payload)
        .then(res => {
          this.logger.debug(`Firebase Response Message ${JSON.stringify(res)}`);
        })
        .catch(err => {
          this.logger.debug(`Firebase Error Message ${JSON.stringify(err)}`);
        });
    }
    await this.alertGateway.updateLocations();
    return true;
  }

  async acceptHelp(body: AcceptHelpUserDto, locale: string): Promise<any> {
    const { requestId, acceptId, message, isPetition } = body;

    const requestUser: User = await this.getUserById(requestId);
    const acceptUser: CustomUserFields = await this.getUserById(acceptId);
    const requestUserlocation = {
      latitude: parseFloat(requestUser.latitude),
      longitude: parseFloat(requestUser.longitude),
    };
    const acceptUserLocation = {
      latitude: parseFloat(acceptUser.latitude),
      longitude: parseFloat(acceptUser.longitude),
    };
    const meters = getDistance(requestUserlocation, acceptUserLocation);
    const ranking = await this.rankingService.getRankingByUser(acceptUser.id);
    const requestPetition = await this.petitionRepo.findOne({
      where: { user_id: requestId, status: 1 },
    });
    acceptUser.petition = requestPetition ? requestPetition : null;
    const language = traslate(requestUser.lang);
    const dataNotification = {
      isRequest: true,
      isAlert: false,
      isPetition,
      id: acceptUser.id,
      type: 'accept',
      title: `${acceptUser.first_name} ${
        acceptUser.last_name ? acceptUser.last_name.charAt(0) : ''
      }`,
      body: isPetition ? language.quiereAyudarte : language.quiereHablarContigo,
      user: { ...acceptUser, meters, ranking },
      smallIcon: 'drawable/icon',
      largeIcon: 'https://avatars2.githubusercontent.com/u/1174345?v=3&s=96',
      autoCancel: true,
      vibrate: [200, 300, 200, 300],
      color: '0000ff',
      headsUp: true,
      sound: true,
    };

    const notification: CreateNotificationDto = {
      user_id: requestUser.id,
      request_user_id: acceptUser.id,
      data: JSON.stringify(dataNotification),
      message_id: '',
      recieved: 0,
      type: 2,
    };
    const notificationRecord = await this.notificationRepository.store(
      notification,
    );

    this.logger.debug(
      `Send Push Notification to user ${requestUser.username} type accept`,
    );
    if (requestUser.fcm) {
      const payload = {
        data: {
          notification: JSON.stringify({
            ...dataNotification,
            user: {
              ...dataNotification.user,
              notificationId: notificationRecord.id,
            },
          }),
          priority: 'high',
        },
        notification: {
          title: `${acceptUser.first_name} ${
            acceptUser.last_name ? acceptUser.last_name.charAt(0) : ''
          }`,
          body: isPetition
            ? language.quiereAyudarte
            : language.quiereHablarContigo,
          content: JSON.stringify(dataNotification),
        },
      };
      await admin
        .messaging()
        .sendToDevice(requestUser.fcm, payload)
        .then(res => {
          if (res.failureCount === 0) {
            this.logger.debug(
              `Firebase Send Push Notification to user ${requestUser.username} type accept`,
            );
          } else {
            this.logger.debug(
              `Firebase Error to Sending Push Notification to user ${
                requestUser.username
              },  error : ${JSON.stringify(res.results[0])}`,
            );
          }
        })
        .catch(err => err);
    } else {
      this.logger.debug(
        `Failed, fcm null to Send FirebasePush Notification to user ${requestUser.username} type accept`,
      );
    }
    return true;
  }

  async updateProfileUser(id: number, body: UpdateProfileUserDto) {
    const {
      first_name,
      last_name,
      age,
      address,
      cp,
      country_id,
      language_id,
      password,
      country_location,
      file,
    } = body;

    const dataUser: any = {
      first_name,
      last_name,
      age: age ? age : 0,
      address,
      cp,
      country_id: country_id ? country_id : 0,
      language_id: language_id ? language_id : 0,
      country_location,
    };
    if (file) {
      const imageName = getImageName(file.name, id);
      await fs.writeFile(
        `src/images/${imageName}`,
        file.data,
        'base64',
        function(err) {},
      );
      dataUser.picture = imageName;
    }

    if (password) {
      const salt = await bcrypt.genSalt();
      const newPassword = await this.userRepository.hashPassword(
        password,
        salt,
      );
      await this.userRepository.update(id, { salt, password: newPassword });
    }

    await this.userRepository.update(id, dataUser);
    const ranking = await this.rankingService.getRankingByUser(id);
    const user = await this.getUserById(id);
    return { ...user, ranking };
  }

  async getUserImage(id: number): Promise<string | null> {
    const user = await this.userRepository.findOne({ where: { id } });
    return user.picture ? user.picture : null;
  }

  checkExpiration(created: Date): boolean {
    const now = moment(new Date());
    const end = moment(created);
    const duration = moment.duration(now.diff(end));
    const hours = duration.asHours();
    return hours > 1 ? true : false;
  }

  checkTemporalNotificationExpiration(created: Date): boolean {
    const now = moment(new Date());
    const end = moment(created);
    const duration = moment.duration(now.diff(end));
    const minutes = duration.minutes();
    return minutes >= 1 ? true : false;
  }

  @Cron('10 * * * * *')
  async handleCronRemovePetition(): Promise<void> {
    const petitions = await this.petitionRepo.find({ where: { status: 1 } });
    for (const item of petitions) {
      const isExpired = this.checkExpiration(item.created);
      if (isExpired) {
        this.petitionRepo.update(item.id, { status: 0 });
        this.alertGateway.updateLocations();
        this.logger.debug(`Petition ${item.description} expired!`);
      }
    }
  }

  async getUserPetition(user_id: number): Promise<Petition> {
    return this.petitionRepo.findOne({ where: { user_id, status: 1 } });
  }

  async disablePetition(id: number) {
    this.petitionRepo.update(id, { status: 0 });
    this.alertGateway.updateLocations();
  }

  public async logout(username: string) {
    const user = await this.userRepository.findOne({ username });
    this.userRepository.update(user.id, { connected: false });
    this.logger.debug(`user ${user.username} disconnected`);
    this.alertGateway.updateLocations();
  }

  async updateUserInterest(body: CreateUserInterestDto) {
    await this.userInterestService.store(body);
    const ranking = await this.rankingService.getRankingByUser(body.userId);
    const user: CustomUserFields = await this.getUserById(body.userId);
    delete user.password;
    delete user.salt;
    this.alertGateway.updateLocations();
    return { ...user, ranking };
  }
}
