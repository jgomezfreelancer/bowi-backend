import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  OneToOne,
  JoinColumn,
  OneToMany,
  JoinTable,
} from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { Participant } from 'src/participant/participant.entity';
import { Room } from 'src/room/room.entity';
import { Petition } from 'src/petition/petition.entity';
import { UserLanguages } from 'src/user-languages/user-languages.entity';
import { UserInterest } from 'src/user-interest/user-interest.entity';

@Entity()
@Unique(['username'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  public username: string;

  @Column({ default: null, nullable: true })
  public first_name: string;

  @Column({ default: null, nullable: true })
  public last_name: string;

  @Column({ default: null, nullable: true })
  public password: string;

  @Column({ default: null, nullable: true })
  public salt: string;

  @Column({ default: null, nullable: true })
  public social_auth: number;

  @Column({ default: null, nullable: true })
  public location: number;

  @Column({ default: null, nullable: true })
  public country_location: string;

  @Column({ default: null, nullable: true })
  public profile: number;

  @Column({ default: null, nullable: true })
  public help: number;

  @Column({ default: null, nullable: true })
  public language_id: number;

  @Column({ default: null, nullable: true })
  public nationality_id: number;

  @Column({ default: null, nullable: true })
  public fcm: string;

  @Column({ default: null, nullable: true })
  public latitude: string;

  @Column({ default: null, nullable: true })
  public longitude: string;

  @Column({ default: null, nullable: true })
  public age: number;

  @Column({ default: null, nullable: true })
  public address: string;

  @Column({ default: null, nullable: true })
  public cp: string;

  @Column({ default: null, nullable: true })
  public country_id: number;

  @Column({ default: null, nullable: true })
  public picture: string;

  @Column({ default: null, nullable: true })
  public lang: string;

  @OneToMany(
    type => Participant,
    participant => participant.user,
  )
  public participant: Participant;

  @OneToMany(
    type => Room,
    room => room.responsible,
  )
  public responsible: Room;

  @Column({ default: null, nullable: true })
  public connected: boolean;

  @Column({ default: null, nullable: true })
  public limit_distance: number;

  @Column({ default: null, nullable: true })
  public first_time: boolean;

  @OneToMany(
    type => Room,
    room => room.user_participant,
  )
  public user_participant: Room;

  @OneToMany(
    type => Petition,
    petition => petition.user,
  )
  public userPetition: Petition;

  @OneToMany(
    type => UserLanguages,
    petition => petition.userLanguage,
  )
  public userLanguages: UserLanguages;

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }

  @OneToMany(
    () => UserInterest,
    userInterest => userInterest.user,
  )
  public userToInterest!: UserInterest[];
}
