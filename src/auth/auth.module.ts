import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { JwtAuthGuard } from './Guards/auth-jwt.guard';

import { ENV } from '../env';
import { NotificationRepository } from 'src/notification/notification.repository';
import { AlertController } from 'src/alert/alert.controller';
import { AlertGateway } from 'src/alert/alert.gateway';
import { AlertModule } from 'src/alert/alert.module';
import { RankingService } from 'src/ranking/ranking.service';
import { RankingModule } from 'src/ranking/ranking.module';
import { UserLanguagesRepository } from 'src/user-languages/user-languages.repository';
import { UserNationalitiesRepository } from 'src/user-nationalities/user-nationalities.respository';
import { PetitionRepository } from 'src/petition/petition.repository';
import { RoomRepository } from 'src/room/room.repository';
import { UserInterestService } from 'src/user-interest/user-interest.service';
import { UserInterestRepository } from 'src/user-interest/user-interest.reposittory';
import { InterestRepository } from 'src/interest/interest.repository';
import { UserBlockRepository } from 'src/user-block/user-block.repository';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: ENV.jwt.secret,
      signOptions: {
        expiresIn: ENV.jwt.expiresIn,
      },
    }),
    TypeOrmModule.forFeature([
      UserRepository,
      NotificationRepository,
      UserLanguagesRepository,
      UserNationalitiesRepository,
      PetitionRepository,
      RoomRepository,
      UserInterestRepository,
      InterestRepository,
      UserBlockRepository,
    ]),
    AlertModule,
    RankingModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, JwtAuthGuard, UserInterestService],
  exports: [JwtStrategy, PassportModule],
})
export class AuthModule {}
