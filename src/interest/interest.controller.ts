import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateInterestDto } from './dto/create-interest.dto';
import { Interest } from './interest.entity';
import { InterestService } from './interest.service';

@Controller('interest')
export class InterestController {
  constructor(private service: InterestService) {}
  @Get('/')
  async index(): Promise<Interest[]> {
    return this.service.index();
  }

  @Post('/')
  async store(@Body() body: CreateInterestDto): Promise<Interest> {
    return this.service.store(body);
  }
}
