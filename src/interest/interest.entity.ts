import { UserInterest } from 'src/user-interest/user-interest.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  Unique,
  BaseEntity,
} from 'typeorm';

@Entity()
@Unique(['description'])
export class Interest extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true, default: null })
  description: string;

  @Column({ nullable: true, default: null })
  language: string;

  @OneToMany(
    () => UserInterest,
    userInterest => userInterest.interest,
  )
  public userToInterest!: UserInterest[];
}
