import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateInterestDto } from './dto/create-interest.dto';
import { Interest } from './interest.entity';
import { InterestRepository } from './interest.repository';

@Injectable()
export class InterestService {
  constructor(
    @InjectRepository(InterestRepository)
    private repo: InterestRepository,
  ) {}

  async index(): Promise<Interest[]> {
    return this.repo.find({ order: { description: 'ASC' } });
  }

  async store(body: CreateInterestDto): Promise<Interest> {
    return this.repo.store(body);
  }
}
