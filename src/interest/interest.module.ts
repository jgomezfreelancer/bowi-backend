import { Module } from '@nestjs/common';
import { InterestService } from './interest.service';
import { InterestController } from './interest.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InterestRepository } from './interest.repository';

@Module({
  imports: [TypeOrmModule.forFeature([InterestRepository])],
  providers: [InterestService],
  controllers: [InterestController],
})
export class InterestModule {}
