import { IsNotEmpty } from 'class-validator';

export class CreateInterestDto {
  @IsNotEmpty()
  description: string;
}
