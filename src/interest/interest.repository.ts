import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { Interest } from './interest.entity';
import { CreateInterestDto } from './dto/create-interest.dto';

@EntityRepository(Interest)
export class InterestRepository extends Repository<Interest> {
  async store(body: CreateInterestDto): Promise<Interest> {
    const { description } = body;

    const interest = new Interest();
    interest.description = description;
    try {
      return interest.save();
    } catch (error) {
      if (error.code === '23505') {
        // duplicate
        throw new ConflictException('Interest already exists');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }
}
