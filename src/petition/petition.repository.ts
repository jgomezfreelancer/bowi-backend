import { EntityRepository, Repository } from 'typeorm';
import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { Petition } from './petition.entity';
import { CreatePetitionDto } from './dto/create-petition.dto';

@EntityRepository(Petition)
export class PetitionRepository extends Repository<Petition> {
  async store(body: CreatePetitionDto): Promise<Petition> {
    const { user_id, description } = body;

    const pendindPetition = this.findOne({ user_id, status: 1 });

    if(pendindPetition) {
        throw new ConflictException('Tiene una solicitud de ayuda pendiente');
    }

    const petition = new Petition();
    petition.user_id = user_id;
    petition.description = description;
    petition.status = 1;
    return petition.save()
  }
}
