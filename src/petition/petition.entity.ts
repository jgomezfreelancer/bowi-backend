import { User } from 'src/auth/user.entity';
import { Room } from 'src/room/room.entity';
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, OneToOne } from 'typeorm';

@Entity()
export class Petition extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true, default: null })
  user_id: number;

  @Column({ nullable: true, default: null })
  description: string;
  
  @Column({ nullable: true, default: null })
  status: number;
  
  @Column('timestamp with time zone')
  created: Date = new Date();
  
  @ManyToOne(type => User, user => user.userPetition, { cascade: true })
  @JoinColumn({ name: "user_id" })
  user: User;
}
