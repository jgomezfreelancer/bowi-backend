import { IsNotEmpty } from 'class-validator';

export class CreatePetitionDto {
  @IsNotEmpty()
  user_id: number;

  @IsNotEmpty()
  description: string;
}
