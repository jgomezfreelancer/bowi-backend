import { Body, Controller, Post } from '@nestjs/common';
import { ChatGateway, IPayloadMessage } from './chat.gateway';

@Controller('chat')
export class ChatController {
  constructor(private chatGateway: ChatGateway) {}

  @Post('/send-manual-message')
  sendManualMessage(@Body() body: IPayloadMessage) {
    this.chatGateway.handleMessage('', body);
  }
}
