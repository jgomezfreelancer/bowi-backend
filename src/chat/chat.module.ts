import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AlertGateway } from 'src/alert/alert.gateway';
import { MessageRepository } from 'src/message/message.repository';
import { ParticipantRepository } from 'src/participant/participant.repository';
import { RoomRepository } from 'src/room/room.repository';
import { ChatGateway } from './chat.gateway';
import { ChatController } from './chat.controller';
import { UserRepository } from 'src/auth/user.repository';
import { NotificationRepository } from 'src/notification/notification.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      MessageRepository,
      RoomRepository,
      ParticipantRepository,
      UserRepository,
      NotificationRepository,
    ]),
  ],
  providers: [ChatGateway, AlertGateway],
  controllers: [ChatController],
  exports: [ChatGateway],
})
export class ChatModule {}
