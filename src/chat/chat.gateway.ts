import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MessageRepository } from 'src/message/message.repository';
import { RoomRepository } from 'src/room/room.repository';
import { AlertGateway } from 'src/alert/alert.gateway';

export interface IPayloadMessage {
  userId: number;
  roomDescription: string;
  roomId: number;
  message: string;
  username: string;
  isMultimedia: boolean;
}
@WebSocketGateway({ namespace: '/chat-room' })
export class ChatGateway implements OnGatewayInit {
  constructor(
    @InjectRepository(MessageRepository)
    private messageRepo: MessageRepository,
    @InjectRepository(RoomRepository)
    private roomRepo: RoomRepository,
    private alertGateway: AlertGateway,
  ) {}

  @WebSocketServer() wss: Server;

  private logger: Logger = new Logger('ChatGateway');

  afterInit(server: any) {
    this.logger.log('Initialized!');
  }

  @SubscribeMessage('chatToServer')
  async handleMessage(client: Socket, payload: IPayloadMessage) {
    const newMessage = await this.messageRepo
      .create({
        room_id: payload.roomId,
        user_id: payload.userId,
        description: payload.message,
        status_view: 0,
        isMultimedia: payload.isMultimedia,
      })
      .save();
    const messages = await this.messageRepo
      .createQueryBuilder('message')
      .where('room_id = :room', { room: payload.roomId })
      .leftJoinAndSelect('message.user', 'user')
      .orderBy('created', 'ASC')
      .getMany();
    this.wss.to(payload.roomDescription).emit('chatToClient', messages);
    this.alertGateway.incomingChatMessageRooms({
      room: payload.roomId,
      userId: payload.userId,
    });
    this.alertGateway.incomingMessage({
      room: payload.roomId,
      userId: payload.userId,
      message: payload.message,
      isMultimedia: payload.isMultimedia,
    });
    this.logger.log(
      `Message sent by user ${payload.username} from Chat Room ${
        payload.roomDescription
      } -- message: ${payload.isMultimedia ? 'Photo' : payload.message}`,
    );
  }

  @SubscribeMessage('joinRoom')
  handleRoomJoin(client: Socket, payload: { room: string; username: string }) {
    client.join(payload.room);
    this.logger.log(
      `User ${payload.username} has joined the Chat Room ${payload.room}`,
    );
    client.emit('joinedRoom', payload.room);
  }

  @SubscribeMessage('leaveRoom')
  handleRoomLeave(client: Socket, payload: { room: string; username: string }) {
    client.leave(payload.room);
    this.logger.log(
      `User ${payload.username} has left the Chat Room ${payload.room}`,
    );
    client.emit('leftRoom', payload.room);
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  handleConnection(client: Socket, ...args: any[]) {
    this.logger.log(`Client connected: ${client.id}`);
  }

  disableClientRoom(payload: { room: string; username: string }) {
    this.logger.log(`Disable room to user : ${payload.username}`);
    this.wss.to(payload.room).emit('clientDisableRoom', { status: true });
  }

  forcedLeftRoom(payload: { room: string; username: string }) {
    this.logger.log(`Force leave room to user : ${payload.username}`);
    this.wss.to(payload.room).emit('clientForceRoom', { status: true });
  }
}
