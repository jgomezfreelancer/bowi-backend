import { EntityRepository, Repository } from 'typeorm';
import { Notification } from './notification.entity';
import { CreateNotificationDto } from './dto/create.dto';

@EntityRepository(Notification)
export class NotificationRepository extends Repository<Notification> {

  async store( body: CreateNotificationDto): Promise<Notification> {
    const { user_id, request_user_id ,data, message_id, recieved, type } = body;
    const notification = new Notification();
    notification.user_id = user_id;
    notification.request_user_id = request_user_id;
    notification.data = data;
    notification.message_id = message_id;
    notification.recieved = recieved;
    notification.type = type;
    return notification.save();
  }

}
