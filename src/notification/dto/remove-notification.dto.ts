import { IsNotEmpty } from 'class-validator';

export class RemoveNotificationDto {

  @IsNotEmpty()
  requestId: number;

  @IsNotEmpty()
  acceptId?: number;

}
