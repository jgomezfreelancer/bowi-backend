import { IsNotEmpty } from 'class-validator';

export class CreateNotificationDto {

  @IsNotEmpty()
  user_id: number;

  @IsNotEmpty()
  request_user_id?: number;

  @IsNotEmpty()
  data: string;

  @IsNotEmpty()
  message_id: string;

  @IsNotEmpty()
  recieved: number;

  @IsNotEmpty()
  type: number;
}
