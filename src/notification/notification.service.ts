import { Injectable, Logger } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';

import { NotificationRepository } from './notification.repository';
import { Notification } from './notification.entity';
import { RemoveNotificationDto } from './dto/remove-notification.dto';

@Injectable()
export class NotificationService {
  private logger = new Logger('NotificationService');

  constructor(
    @InjectRepository(NotificationRepository)
    private repo: NotificationRepository,
  ) {}

  async setRecievedNotification(id: number) {
    const found = await this.repo.findOne({ where: { id } });
    if (found) {
      this.repo.update(found.id, { recieved: 1 });
      this.logger.debug(`Notificacion id: ${found.id} Synchronized`);
    }
  }

  async remove(body: RemoveNotificationDto) {
    const { requestId, acceptId } = body;
    const found = await this.repo.findOne({ where: { user_id: requestId, request_user_id: acceptId, recieved: 1 } });
    if (found) {
      const notificationsToRemove = await this.repo.find({ where: { user_id: requestId, request_user_id: acceptId } });
      this.repo.remove(notificationsToRemove)
      this.logger.debug(`Pending Notificacions removed`);
    }
  }

  async getFailureNotification(user_id: number): Promise<Notification | {}> {
    
    let notification: Notification = await this.repo.findOne({ where: { user_id, recieved: 0 }, order: { created: 'DESC' } })
    if (notification) {
      if (
        moment().format('YYYY-MM-DD') ===
        moment(notification.created).format('YYYY-MM-DD')
      ) {
        const created = moment(notification.created);
        const minutes =  moment().diff(created, 'minutes');
        if (minutes <= 30) {
          notification.data = JSON.parse(notification.data);
          this.logger.debug(`getFailureNotification to user id: ${user_id}`);
          return notification;
        }
      }
    }

    return null;
  }

  async checkPendingNotification(requestId: number, acceptId: number): Promise<boolean> {
    const pendingNotification = await this.repo.findOne({
      user_id: requestId,
      request_user_id: acceptId,
      recieved: 1,
    });
    return !!pendingNotification;
  }
}
