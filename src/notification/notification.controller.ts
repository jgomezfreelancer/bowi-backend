import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Query, ValidationPipe } from '@nestjs/common';
import { RemoveNotificationDto } from './dto/remove-notification.dto';
import { NotificationService } from './notification.service';

@Controller('notification')
export class NotificationController {
    constructor(
        private service: NotificationService,
      ) {}

     @Put('/recieved')
     async setRecievedNotification(@Body('id') id: number) {
         return this.service.setRecievedNotification(id);
     }

     @Get('/failure/:id')
     async getFailureNotification(@Param('id', ParseIntPipe) id: number): Promise<void | {}> {
         return this.service.getFailureNotification(id);
     }

     @Post('/remove')
     async remove(@Body() body: RemoveNotificationDto): Promise<void | {}> {
         return this.service.remove(body);
     }

     @Get('/check-pending')
     async checkPendingNotification(@Query(ValidationPipe) body: { requestId: number, acceptId: number },): Promise<boolean> {
        return this.service.checkPendingNotification(body.requestId, body.acceptId);
     }
}
