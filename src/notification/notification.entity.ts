import { BaseEntity, Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Notification extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
    
  @Column({ nullable: true, default: null })
  user_id: number;

  @Column({ nullable: true, default: null })
  request_user_id: number;

  @Column({ nullable: true, default: null })
  data: string;

  @Column({ nullable: true, default: null })
  message_id: string;

  @Column({ nullable: true, default: null })
  recieved: number;

  @Column({ nullable: true, default: null })
  type: number;

  @Column('timestamp with time zone')
  created: Date = new Date();
}

