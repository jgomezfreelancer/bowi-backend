import { Module } from '@nestjs/common';
import { UserBlockService } from './user-block.service';
import { UserBlockController } from './user-block.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserBlockRepository } from './user-block.repository';

@Module({
  imports: [TypeOrmModule.forFeature([UserBlockRepository])],
  providers: [UserBlockService],
  controllers: [UserBlockController],
})
export class UserBlockModule {}
