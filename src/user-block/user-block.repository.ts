import { EntityRepository, Repository } from 'typeorm';
import { UserBlock } from './user-block.entity';

@EntityRepository(UserBlock)
export class UserBlockRepository extends Repository<UserBlock> {}
