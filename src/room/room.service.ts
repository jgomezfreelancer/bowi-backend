import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateRoomDto } from './dto/create-room.dto';
import { Room } from './room.entity';
import { RoomRepository } from './room.repository';
import * as moment from 'moment';
import * as fs from 'fs';

import { ParticipantRepository } from 'src/participant/participant.repository';
import { MessageRepository } from 'src/message/message.repository';
import { AlertGateway, CustomRoomFields } from 'src/alert/alert.gateway';
import { Participant } from 'src/participant/participant.entity';
import { ChatGateway } from 'src/chat/chat.gateway';
import { UserRepository } from 'src/auth/user.repository';
import { PetitionRepository } from 'src/petition/petition.repository';
import { Cron } from '@nestjs/schedule';
import { User } from 'src/auth/user.entity';
import { BlockUserRoomDto } from './dto/block-user-room.dto';
import { UserBlockRepository } from 'src/user-block/user-block.repository';
import { traslate } from 'src/helpers/traslate';

@Injectable()
export class RoomService {
  private logger = new Logger('RoomService');
  constructor(
    @InjectRepository(RoomRepository)
    private repo: RoomRepository,
    @InjectRepository(ParticipantRepository)
    private participantRepo: ParticipantRepository,
    @InjectRepository(MessageRepository)
    private messageRepo: MessageRepository,
    @InjectRepository(UserRepository)
    private userRepo: UserRepository,
    private alertGateway: AlertGateway,
    private chatGateway: ChatGateway,
    @InjectRepository(PetitionRepository)
    private petitionRepo: PetitionRepository,
    @InjectRepository(UserBlockRepository)
    private userBlockRepo: UserBlockRepository,
  ) {}

  async index(): Promise<Room[]> {
    return this.repo.find();
  }

  async checkInverseParticipant(
    roomId: number,
    user: number,
  ): Promise<User | null> {
    const roomResponsible = await this.repo.findOne({
      where: { id: roomId, participant_id: user },
      relations: ['responsible'],
    });
    if (roomResponsible) {
      return roomResponsible.responsible;
    }
    const roomParticipant = await this.repo.findOne({
      where: { id: roomId, responsible_id: user },
      relations: ['user_participant'],
    });
    if (roomParticipant) {
      return roomParticipant.user_participant;
    }
    return null;
  }

  async getCountPendingViews(room: number, user: number): Promise<number> {
    const reciever = await this.checkInverseParticipant(room, user);
    if (reciever) {
      const countPendingViews = await this.messageRepo.find({
        room_id: room,
        user_id: reciever.id,
        status_view: 0,
      });
      return countPendingViews.length;
    }
    return 0;
  }

  async getProfileImage(image: string): Promise<string | null> {
    if (image !== null) {
      try {
        const data: any = await fs.readFileSync(`./src/images/${image}`);
        const base64 = Buffer.from(data).toString('base64');
        const image64 = `data:image/png;base64,${base64}`;
        return image64;
      } catch (error) {
        return null;
      }
    }
    return null;
  }

  async getByUser(user: number): Promise<void[]> {
    const userRooms: CustomRoomFields[] = await this.repo.find({
      relations: ['petition', 'responsible', 'user_participant'],
      order: { created: 'DESC' },
    });
    const rooms = [];
    for (const [index, item] of userRooms.entries()) {
      const userId: number | null =
        item.responsible_id == user
          ? item.participant_id
          : item.participant_id == user
          ? item.responsible_id
          : null;
      if (userId) {
        const userRoom = await this.userRepo.findOne({
          where: { id: userId },
        });
        userRooms[index].user = userRoom;
        userRooms[index].userImage = await this.getProfileImage(
          userRoom.picture,
        );
        const message = await this.messageRepo.findOne({
          where: { room_id: item.id },
          order: { created: 'DESC' },
        });
        userRooms[index].lastMessage = message;
        userRooms[index].pendingViews = await this.getCountPendingViews(
          item.id,
          user,
        );
        if (item.petition) {
          const userPetition = await this.userRepo.findOne({
            where: { id: item.petition.user_id },
          });
          userRooms[index].userPetition = userPetition;
        }

        rooms.push(userRooms[index]);
      } else {
        delete userRooms[index];
      }
    }
    return rooms ? rooms : [];
  }

  async store(body: CreateRoomDto, locale: string): Promise<Room> {
    const { responsibleId, participantId, petitionId } = body;
    const room = await this.repo.store(body);
    const roomDescription = `room#${room.id}`;

    await this.repo.update(room.id, {
      description: roomDescription,
      petition_id: petitionId ? petitionId : null,
    });

    await this.participantRepo
      .create({
        room_id: room.id,
        user_id: responsibleId,
      })
      .save();
    await this.participantRepo
      .create({
        room_id: room.id,
        user_id: participantId,
      })
      .save();

    const currentRoom: CustomRoomFields = await this.repo.findOne({
      where: { id: room.id },
      relations: ['user_participant', 'responsible', 'petition'],
    });
    if (currentRoom.petition) {
      const userPetition = await this.userRepo.findOne({
        where: { id: currentRoom.petition.user_id },
      });
      currentRoom.userPetition = userPetition;
    }

    this.alertGateway.incomingParticipantMessage(
      currentRoom,
      petitionId ? currentRoom.user_participant : currentRoom.responsible,
      petitionId ? currentRoom.responsible : currentRoom.user_participant,
      locale,
    );
    this.logger.debug(`New room: ${currentRoom.id}`);
    return currentRoom;
  }

  async delete(id: number): Promise<boolean> {
    const room = await this.repo.findOne({
      where: { id },
      relations: ['user_participant', 'responsible'],
    });

    await this.messageRepo
      .createQueryBuilder()
      .delete()
      .where('room_id = :id', { id })
      .execute();

    await this.participantRepo
      .createQueryBuilder()
      .delete()
      .where('room_id = :id', { id })
      .execute();

    this.chatGateway.forcedLeftRoom({
      room: room.description,
      username: room.responsible.username,
    });

    this.chatGateway.forcedLeftRoom({
      room: room.description,
      username: room.user_participant.username,
    });

    await this.repo.delete({ id });

    this.alertGateway.refreshRoomList(
      room.responsible.id,
      room.user_participant.username,
    );

    this.alertGateway.refreshRoomList(
      room.user_participant.id,
      room.user_participant.username,
    );

    return true;
  }

  async disableRoom(id: number) {
    const room = await this.repo.findOne({
      relations: ['user_participant', 'responsible'],
      where: { id },
    });
    this.chatGateway.disableClientRoom({
      room: room.description,
      username: room.user_participant.username,
    });
    this.alertGateway.refreshRoomList(
      room.user_participant.id,
      room.user_participant.username,
    );

    this.chatGateway.disableClientRoom({
      room: room.description,
      username: room.responsible.username,
    });
    this.alertGateway.refreshRoomList(
      room.responsible.id,
      room.responsible.username,
    );
    return this.repo.update(id, { disable: true });
  }

  async manualDisableRoom(id: number) {
    const room = await this.repo.findOne({
      relations: ['user_participant'],
      where: { id },
    });
    this.chatGateway.disableClientRoom({
      room: room.description,
      username: room.user_participant.username,
    });
    this.alertGateway.refreshRoomList(
      room.user_participant.id,
      room.user_participant.username,
    );
    return this.repo.update(id, { disable: true });
  }

  checkExpiration(created: Date): boolean {
    const now = moment(new Date());
    const end = moment(created);
    const duration = moment.duration(now.diff(end));
    const hours = duration.asHours();
    return hours >= 3 ? true : false;
  }

  @Cron('25 * * * * *')
  async handleCronRemoveRoom() {
    const rooms = await this.repo.find();
    for (const item of rooms) {
      const isExpired = this.checkExpiration(item.created);
      if (isExpired) {
        this.delete(item.id);
        this.logger.debug(`Room ${item.description} expired!`);
      }
    }
  }

  async checkRoomExpiration(id: number): Promise<number> {
    const room = await this.repo.findOne(id);
    const dateRoom = moment(room.created);
    const currentDate = moment(new Date());
    const duration = 24 - currentDate.diff(dateRoom, 'hours');
    return duration;
  }

  async blockUserRoom(body: BlockUserRoomDto) {
    const { authUser, blockUser } = body;
    const checkResponsibleRooms = await this.repo.find({
      where: { responsible_id: authUser, participant_id: blockUser },
    });
    const checkParticipantRooms = await this.repo.find({
      where: { responsible_id: blockUser, participant_id: authUser },
    });

    if (checkResponsibleRooms.length > 0) {
      for (const responsibleRoom of checkResponsibleRooms) {
        await this.disableRoom(responsibleRoom.id);
      }
    }

    if (checkParticipantRooms.length > 0) {
      for (const responsibleRoom of checkParticipantRooms) {
        await this.disableRoom(responsibleRoom.id);
      }
    }

    this.userBlockRepo
      .create({
        authUser,
        blockUser,
      })
      .save();
  }
}
