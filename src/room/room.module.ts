import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AlertGateway } from 'src/alert/alert.gateway';
import { UserRepository } from 'src/auth/user.repository';
import { ChatModule } from 'src/chat/chat.module';
import { MessageRepository } from 'src/message/message.repository';
import { NotificationRepository } from 'src/notification/notification.repository';
import { ParticipantRepository } from 'src/participant/participant.repository';
import { PetitionRepository } from 'src/petition/petition.repository';
import { UserBlockRepository } from 'src/user-block/user-block.repository';
import { RoomController } from './room.controller';
import { RoomRepository } from './room.repository';
import { RoomService } from './room.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      RoomRepository,
      ParticipantRepository,
      MessageRepository,
      UserRepository,
      PetitionRepository,
      NotificationRepository,
      UserBlockRepository,
    ]),
    ChatModule,
  ],
  providers: [RoomService, AlertGateway],
  controllers: [RoomController],
})
export class RoomModule {}
