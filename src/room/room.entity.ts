import { User } from 'src/auth/user.entity';
import { Participant } from 'src/participant/participant.entity';
import { Petition } from 'src/petition/petition.entity';
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, Unique, OneToMany, ManyToOne, JoinColumn, OneToOne } from 'typeorm';

@Entity()
@Unique(['description'])
export class Room extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
    
  @Column({ nullable: true, default: null })
  description: string;

  @Column({ nullable: true, default: null })
  responsible_id: number;

  @Column({ nullable: true, default: null })
  participant_id: number;

  @Column({ nullable: true, default: null })
  petition_id: number;

  @Column({ nullable: true, default: null })
  disable: boolean;

  @ManyToOne(type => User, user => user.participant, { cascade: true })
  @JoinColumn({ name: "responsible_id" })
  responsible: User;


  @ManyToOne(type => User, user => user.participant, { cascade: true })
  @JoinColumn({ name: "participant_id" })
  user_participant: User;

  @ManyToOne(type => Petition, petition => petition.id, { cascade: true })
  @JoinColumn({ name: "petition_id" })
  petition: Petition;

  @Column('timestamp with time zone')
  created: Date = new Date();

  @OneToMany(type => Participant, participant => participant.room)
  public participant: Participant;

}
