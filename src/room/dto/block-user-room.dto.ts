import { IsNotEmpty } from 'class-validator';

export class BlockUserRoomDto {
  @IsNotEmpty()
  authUser: number;

  @IsNotEmpty()
  blockUser: number;
}
