import { IsNotEmpty } from 'class-validator';

export class CreateRoomDto {
  @IsNotEmpty()
  responsibleId: number;

  @IsNotEmpty()
  participantId: number;
  
  @IsNotEmpty()
  petitionId: number;
}
