import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Req,
} from '@nestjs/common';
import { Request } from 'express';
import { Participant } from 'src/participant/participant.entity';
import { BlockUserRoomDto } from './dto/block-user-room.dto';
import { CreateRoomDto } from './dto/create-room.dto';
import { Room } from './room.entity';
import { RoomService } from './room.service';

@Controller('room')
export class RoomController {
  constructor(private service: RoomService) {}

  @Get('/')
  async index(): Promise<Room[]> {
    return this.service.index();
  }

  @Get('/get-by-user/:id')
  async getRoomsByUser(@Param('id') user: number): Promise<void[]> {
    return this.service.getByUser(user);
  }

  @Post('/')
  async store(@Body() body: CreateRoomDto, @Req() req: Request): Promise<Room> {
    const locale: any = req.headers.locale ? req.headers.locale : null;
    return this.service.store(body, locale);
  }

  @Delete('/:id')
  delete(@Param('id', ParseIntPipe) id: number): Promise<boolean> {
    return this.service.delete(id);
  }

  @Put('/disable/:id')
  async disableRoom(@Param('id') id: number) {
    return this.service.disableRoom(id);
  }

  @Get('/check-expiration/:id')
  async checkRoomExpiration(@Param('id') id: number): Promise<number> {
    return this.service.checkRoomExpiration(id);
  }

  @Post('/block-user')
  async blockUserRoom(@Body() body: BlockUserRoomDto): Promise<any> {
    return this.service.blockUserRoom(body);
  }
}
