import { EntityRepository, Repository } from 'typeorm';
import { ConflictException, InternalServerErrorException } from '@nestjs/common';
import { Room } from './room.entity';
import { CreateRoomDto } from './dto/create-room.dto';

@EntityRepository(Room)
export class RoomRepository extends Repository<Room> {

  async store( body: CreateRoomDto): Promise<Room> {
      const { responsibleId, participantId, petitionId } = body;
    const room = new Room();
    room.responsible_id = responsibleId;
    room.participant_id = participantId;
    room.petition_id = petitionId;
    room.disable = false;
    try {
        return room.save();
      } catch (error) {
        if (error.code === '23505') { // duplicate
          throw new ConflictException('Room already exists');
        } else {
          throw new InternalServerErrorException();
        }
      }
  }

}
