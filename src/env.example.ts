interface IServer {
    port: number;
}

interface IJwt {
    expiresIn: number;
    secret: string;
}

interface IFirebase {
    projectId: string;
    privateKey: string;
    clientEmail: string;
}


interface IDB {
    host: string
    type: 'postgres' | 'mysql';
    port: number;
    database: string;
    username: string;
    password:  string;
}

interface IEnv {
    db: IDB,
    server: IServer,
    jwt: IJwt;
    firebase: IFirebase
}

export const ENV: IEnv = {
    // Server Config
    server: { port: 8000 },

    // Database Config
    db: {
        type: 'postgres',
        host: 'localhost',
        port: 5432,
        username: '',
        password: '',
        database: '',
    },

    // Auth JWT
    jwt: {
        expiresIn: 3600,
        secret: ''
    },

    // Firebase Credentials
    firebase: {
        projectId: "",
        privateKey: "",
        clientEmail: "",
    }
}