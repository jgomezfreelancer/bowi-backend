import { IsNumber, IsString } from 'class-validator';

export class ReportUserDto {
  @IsNumber()
  authUser: number;

  @IsNumber()
  reportUser: number;

  @IsString()
  reason: string;
}
