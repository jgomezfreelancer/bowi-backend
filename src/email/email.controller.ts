import { Body, Controller, Get, Req, Post, Query } from '@nestjs/common';
import { ReportUserDto } from './dto/report-user.dto';
import { EmailService } from './email.service';
import { Request } from 'express';

@Controller('email')
export class EmailController {
  constructor(private emailService: EmailService) {}

  @Get('/forgot-password')
  async forgotPassword(
    @Query('username') username: string,
  ): Promise<{ message: string }> {
    return this.emailService.sendMail(username);
  }

  @Post('/report-user')
  async reportUser(
    @Body() body: ReportUserDto,
    @Req() req: Request,
  ): Promise<{ message: string }> {
    const baseUrl = `${req.protocol}://${req.headers.host}`;
    return this.emailService.sendReportMail(body, baseUrl);
  }
}
