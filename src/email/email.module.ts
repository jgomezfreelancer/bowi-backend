import { Module } from '@nestjs/common';
import { EmailService } from './email.service';
import { EmailController } from './email.controller';
import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/auth/user.repository';

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        service: 'gmail',
        port: 587,
        ignoreTLS:true,
        requireTLS:false,
        secure: false,
        auth: {
          user: 'jorge6242@gmail.com',
          pass: 'nkzkarcruqayfelm',
        },
      },
      defaults: {
        from: '"No Reply" <no-reply@localhost>',
      },
      template: {
        dir: __dirname + '/templates',
        adapter: new PugAdapter(),
        options: {
          strict: true,
        },
      },
    }),
    TypeOrmModule.forFeature([UserRepository])
  ],
  providers: [EmailService],
  controllers: [EmailController]
})
export class EmailModule {}
