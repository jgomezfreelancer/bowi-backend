import { IsNotEmpty } from 'class-validator';

export class CreateUserInterestDto {
  @IsNotEmpty()
  userId: number;

  @IsNotEmpty()
  interests: number[];
}
