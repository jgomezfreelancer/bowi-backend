import { Module } from '@nestjs/common';
import { UserInterestService } from './user-interest.service';
import { UserInterestController } from './user-interest.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserInterestRepository } from './user-interest.reposittory';
import { InterestRepository } from 'src/interest/interest.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserInterestRepository, InterestRepository]),
  ],
  providers: [UserInterestService],
  controllers: [UserInterestController],
})
export class UserInterestModule {}
