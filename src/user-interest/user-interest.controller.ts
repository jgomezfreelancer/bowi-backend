import { Body, Controller, Post } from '@nestjs/common';
import { CreateUserInterestDto } from './dto/create-user-interest.dto';
import { UserInterest } from './user-interest.entity';
import { UserInterestService } from './user-interest.service';

@Controller('user-interest')
export class UserInterestController {
  constructor(private service: UserInterestService) {}
  @Post('/')
  async store(@Body() body: CreateUserInterestDto): Promise<UserInterest[]> {
    return this.service.store(body);
  }
}
