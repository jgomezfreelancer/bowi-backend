import {
  Entity,
  Column,
  ManyToOne,
  PrimaryGeneratedColumn,
  BaseEntity,
} from 'typeorm';
import { User } from 'src/auth/user.entity';
import { Interest } from 'src/interest/interest.entity';

@Entity()
export class UserInterest extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column()
  public userId!: number;

  @Column()
  public interestId!: number;

  @ManyToOne(
    () => User,
    user => user.userToInterest,
  )
  public user!: User;

  @ManyToOne(
    () => Interest,
    interest => interest.userToInterest,
  )
  public interest!: Interest;
}
