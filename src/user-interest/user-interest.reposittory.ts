import { EntityRepository, Repository } from 'typeorm';
import { UserInterest } from './user-interest.entity';

@EntityRepository(UserInterest)
export class UserInterestRepository extends Repository<UserInterest> {}
