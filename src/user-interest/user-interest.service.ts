import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InterestRepository } from 'src/interest/interest.repository';
import { CreateUserInterestDto } from './dto/create-user-interest.dto';
import { UserInterest } from './user-interest.entity';
import { UserInterestRepository } from './user-interest.reposittory';

@Injectable()
export class UserInterestService {
  constructor(
    @InjectRepository(UserInterestRepository)
    private repo: UserInterestRepository,
    @InjectRepository(InterestRepository)
    private interestRepo: InterestRepository,
  ) {}

  async store(body: CreateUserInterestDto): Promise<UserInterest[]> {
    const { userId, interests } = body;
    const currentUserInterest = await this.repo.findOne({ userId });
    if (currentUserInterest) {
      this.repo.delete({ userId });
    }
    for (const item of interests) {
      await this.repo
        .create({
          userId,
          interestId: item,
        })
        .save();
    }
    return this.repo.find({ where: { userId } });
  }

  async getByUser(userId: number): Promise<UserInterest[]> {
    return this.repo.find({ where: { userId } });
  }
}
