import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
// Import firebase-admin
import * as admin from 'firebase-admin';
import { ServiceAccount } from 'firebase-admin';
import * as bodyParser from 'body-parser';
import * as express from 'express';
import { join } from 'path';

import { ENV } from './env';

async function bootstrap() {
  const server = ENV.server;
  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(AppModule);
  app.use(express.static(join('./src/images')));
  const adminConfig: ServiceAccount = {
    projectId: ENV.firebase.projectId,
    privateKey: ENV.firebase.privateKey,
    clientEmail: ENV.firebase.clientEmail,
  };
  // Initialize the firebase admin app
  admin.initializeApp({
    credential: admin.credential.cert(adminConfig),
    databaseURL: 'https://app-help-62efe.firebaseio.com',
  });
  app.enableCors();
  app.setGlobalPrefix('api/v1');
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  await app.listen(server.port);
  logger.log(`Application listening on port ${server.port}`);
}
bootstrap();
