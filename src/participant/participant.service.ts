import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateParticipantDto } from './dto/create-participant.dto';
import { Participant } from './participant.entity';
import { ParticipantRepository } from './participant.repository';

@Injectable()
export class ParticipantService {
  constructor(
    @InjectRepository(ParticipantRepository)
    private repo: ParticipantRepository,
  ) {}

  async index(): Promise<Participant[]> {
    return this.repo.find();
  }

  async getByUser(user: number): Promise<Participant[]> {
    return this.repo.find({ where: { user_id: user } });
  }

  async store(body: CreateParticipantDto): Promise<Participant> {
    return this.repo.store(body);
  }
}
