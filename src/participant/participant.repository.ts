import { EntityRepository, Repository } from 'typeorm';
import { InternalServerErrorException } from '@nestjs/common';
import { Participant } from './participant.entity';
import { CreateParticipantDto } from './dto/create-participant.dto';

@EntityRepository(Participant)
export class ParticipantRepository extends Repository<Participant> {

  async store( body: CreateParticipantDto): Promise<Participant> {
    const { room_id, user_id } = body;

    const room = new Participant();
    room.room_id = room_id;
    room.user_id = user_id;
    try {
        return room.save();
      } catch (error) {     
          throw new InternalServerErrorException();
      }
  }

}
