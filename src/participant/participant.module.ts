import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParticipantController } from './participant.controller';
import { ParticipantRepository } from './participant.repository';
import { ParticipantService } from './participant.service';

@Module({
  imports: [TypeOrmModule.forFeature([ParticipantRepository])],
  providers: [ParticipantService],
  controllers: [ParticipantController],
})
export class ParticipantModule {}
