import { IsNotEmpty } from 'class-validator';

export class CreateParticipantDto {
  @IsNotEmpty()
  room_id: number;

  @IsNotEmpty()
  user_id: number;
}
