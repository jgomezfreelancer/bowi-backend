import { User } from 'src/auth/user.entity';
import { Room } from 'src/room/room.entity';
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, Unique, OneToOne, JoinColumn, Index, ManyToOne } from 'typeorm';

@Entity()
export class Participant extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
    
  @Column()
  room_id: number;

  @Column({ nullable: true, default: null })
  @Index({ unique: false })
  user_id: number;

  @ManyToOne(type => Room, room => room.participant, { cascade: true })
  @JoinColumn({ name: "room_id" })
  room: Room;

  @ManyToOne(type => User, user => user.participant, { cascade: true })
  @JoinColumn({ name: "user_id" })
  user: User;

  @Column('timestamp with time zone')
  created: Date = new Date();

}
