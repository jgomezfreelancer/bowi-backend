interface IServer {
  port: number;
}

interface IJwt {
  expiresIn: number;
  secret: string;
}

interface IFirebase {
  projectId: string;
  privateKey: string;
  clientEmail: string;
}

interface IDB {
  host: string;
  type: 'postgres' | 'mysql';
  port: number;
  database: string;
  username: string;
  password: string;
}

interface IEnv {
  db: IDB;
  server: IServer;
  jwt: IJwt;
  firebase: IFirebase;
}

export const ENV: IEnv = {
  // Server Config
  server: { port: 8001 },

  // Database Config
  db: {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'helpuser',
    password: 'helpuser.123',
    database: 'help',
  },

  // Auth JWT
  jwt: {
    expiresIn: 360000000,
    secret: 'helpapp123',
  },

  // Firebase Credentials
  firebase: {
    projectId: 'bowi-e025b',
    privateKey:
      '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCRoB58glbvlLS5\nCJeB4IuCSVA3L12BfCcqiyItv99F7cBofj0vlBMG99atNOwPr9kkFip5agoCV5Ta\nsVh/cBxVEV8VIcTZL1BQgJUVcYc+SGmp0pwtuliUyBJ1jeODpq7m69debsb7+l6P\nb3+wYEGuxkcpS9gBueexX7a/M1wGUJpSR54MGFPo7+kC+gizBhI9jrDYHSkhOphW\nmZGH2Uaezh7FhfKESw97vIP1NaWUnAbxSOiLH9RFqbgrc+abfppVWxBWyfsvo7Wf\nUAnW+0MAN0fdnYthfgBZBvNNk1t46QWrgIokg/TQc25LSNWC2DZYWC3iFD39KofQ\nJkX7jhONAgMBAAECggEACPbktDYd0z/B5EYSyuGfaj9zGwKPwgHPiL8/NQvDsH1T\nQ9t0OXKRvUhnxF/RTEyUlF5yR5PEgU0SPRLVEiXnneuk1nCED/k+phtUPMQGrEnZ\nifpiDNqUy2E0PwejWB5cU/DJYDx3hAQZ6sCACW5+J0Z4vk0MmLFGAarqXVjJsxsO\nrhsR4uFcD4gHVojM1hjyBJEvm0Dj/m13rT9+snMaRaJ0GhocKbN0KUM/i73Wvdue\nsQ2cbbLd6mUCopcjRvlcgaQiMeHYKITGJFriY+AgACkrWVQ4jYbbJQJZaFKgq5lN\ncEjxERXPFbuy66HzuoHuX6VICrdGw+pC4xV+Xx0sgQKBgQDFB22SPstVsrIjJuln\nkJ6IUmuNyGBakrEPvtTQvsLMSbJ8O3w789yjb+pqqbodPUC02oePGAxeUNw3ovHk\nzL+kw0aL/YyU5hqeZn91C+pIQ22AzUhXSLVWrItjs4usRs5xJE5Ts9zet8Y5w9Gi\nHFiDks+z1hjL7hmewng0BMl+4QKBgQC9Nhi4Jlr0+bhrUMAlAEeB9D2hDk9aEywD\n0ezRahDI1+glpR6zNooY6qD0R6ab99rJ6c9HbgCDGJikU4QRMvGWWFvPE6tNOHL8\ny5BsQrBzWRFi5k91LPqffLY1pI+2PvyrUEh7/EMnjfStieKQwZ8CsFGQvipvd1e3\nykmR/9mGLQKBgBGqXNSj9K82Wmp0brDucohee2427Qyx4lY7EfG1aNnQMcSZwMXP\nU5UDz8zIcvt0Jn0Qqt7VlSZPsa77WRzejsBDEewzS4t+JhzVq8/exZ69DPBjd3z7\n8+ZVihniWz+TCadzTeKLWd5OM+Ox/vW9/OGvJBhHtely67EkVoqzcy2BAoGAPdAI\nb1CIcnOKXfS5cN0Qf1HtOEpWfhS5Ul4oQZmm9hYizvmMd/ufTdiI0IT/1wA3x7LM\ndodnP2yvoS9ve7bq2bTv+Uxxr/57Aj72ZYe74VL9rROUbbFOBwBU/l1OXbliDg1i\nQK7+uVNU1cSv8Z5vdg9n9iNJ9bRFQdOiCUJOK9kCgYEArUHVVe6fvynkN+mRShTC\nnVSXuvxJ/gJnFy/CGlz0lUvXBCrsLbAsw3M9d66c3/ISl0V4/lR5S4BlaF9MqJJj\ng93TCYgrSG03rFTr/+VpWSsv77qwEqtNUAXubhgMH/GK6vFY3Kdn0ezWiawwfWYR\nzNrwTkR0YjVQx6E0I76hyhU=\n-----END PRIVATE KEY-----\n',
    clientEmail: 'firebase-adminsdk-py684@bowi-e025b.iam.gserviceaccount.com',
  },
};
