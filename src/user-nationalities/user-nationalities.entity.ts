import { BaseEntity, Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class UserNationalities extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: null, nullable: true})
  user_id: number;
    
  @Column({ default: null, nullable: true})
  nationality_id: number;
}
