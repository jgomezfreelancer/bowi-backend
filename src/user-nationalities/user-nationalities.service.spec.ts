import { Test, TestingModule } from '@nestjs/testing';
import { UserNationalitiesService } from './user-nationalities.service';

describe('UserNationalitiesService', () => {
  let service: UserNationalitiesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserNationalitiesService],
    }).compile();

    service = module.get<UserNationalitiesService>(UserNationalitiesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
