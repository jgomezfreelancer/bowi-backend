import { EntityRepository, Repository } from 'typeorm';
import { InternalServerErrorException } from '@nestjs/common';
import { UserNationalities } from './user-nationalities.entity';
import { CreateUserNationalitiesDto } from './dto/create-user-nationalities.dto';

@EntityRepository(UserNationalities)
export class UserNationalitiesRepository extends Repository<UserNationalities> {

  async store( body: CreateUserNationalitiesDto): Promise<UserNationalities> {
    const { user_id, nationality_id } = body;

    const language = new UserNationalities();
    language.user_id = user_id;
    language.nationality_id = nationality_id;
    try {
        return language.save();
      } catch (error) {
        throw new InternalServerErrorException();
      }
  }

}
