import { Test, TestingModule } from '@nestjs/testing';
import { UserNationalitiesController } from './user-nationalities.controller';

describe('UserNationalitiesController', () => {
  let controller: UserNationalitiesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserNationalitiesController],
    }).compile();

    controller = module.get<UserNationalitiesController>(UserNationalitiesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
