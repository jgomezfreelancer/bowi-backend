import { IsNotEmpty } from 'class-validator';

export class CreateUserNationalitiesDto {
  @IsNotEmpty()
  user_id: number;

  @IsNotEmpty()
  nationality_id: number;
}
