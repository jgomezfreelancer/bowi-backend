import { Module } from '@nestjs/common';
import { UserNationalitiesController } from './user-nationalities.controller';
import { UserNationalitiesService } from './user-nationalities.service';

@Module({
  controllers: [UserNationalitiesController],
  providers: [UserNationalitiesService]
})
export class UserNationalitiesModule {}
