import { IsNotEmpty } from 'class-validator';

export class CreateUserLanguagesDto {
  @IsNotEmpty()
  user_id: number;

  @IsNotEmpty()
  language_id: number;
}
