import { Module } from '@nestjs/common';
import { UserLanguagesController } from './user-languages.controller';
import { UserLanguagesService } from './user-languages.service';

@Module({
  controllers: [UserLanguagesController],
  providers: [UserLanguagesService]
})
export class UserLanguagesModule {}
