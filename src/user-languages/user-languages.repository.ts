import { EntityRepository, Repository } from 'typeorm';
import { InternalServerErrorException } from '@nestjs/common';
import { UserLanguages } from './user-languages.entity';
import { CreateUserLanguagesDto } from './dto/create-user-languages..dto';

@EntityRepository(UserLanguages)
export class UserLanguagesRepository extends Repository<UserLanguages> {

  async store( body: CreateUserLanguagesDto): Promise<UserLanguages> {
    const { user_id, language_id } = body;

    const language = new UserLanguages();
    language.user_id = user_id;
    language.language_id = language_id;
    try {
        return language.save();
      } catch (error) {
        throw new InternalServerErrorException();
      }
  }

}
