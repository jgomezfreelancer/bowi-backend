import { User } from 'src/auth/user.entity';
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';

@Entity()
export class UserLanguages extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: null, nullable: true})
  user_id: number;
    
  @Column({ default: null, nullable: true})
  language_id: number;

  @ManyToOne(type => User, user => user.userLanguages, { primary: true })
  @JoinColumn({ name: "user_id" })
  userLanguage: User;
}
